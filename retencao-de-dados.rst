.. _retencao:

=============================================
Retenção de Registros de Conexão e Aplicações
=============================================



Marco Civil da Internet
=======================

.. figure:: imagens/retencao_de_dados.png
    :align: center

    Tabela feita pelo Coletivo Saravá [SARAVA-RETENCAO-2014]_


.. _retencao_violacao:

Violação de direitos?
=====================

**Muito se negociou durante os vários anos de debate do Marco Civil da Internet sobre necessidade e prazos para a guarda de registros de conexão e de aplicações de internet até se chegar à situação do quadro acima. A obrigação de guarda registros de aplicação não saiu do texto produzido através das consultas públicas; ela foi introduzida quando o projeto de lei já estava no Congresso , tramitando em regime de urgência, em razão das revelações sobre práticas de vigilância em massa por parte do governo americano e aliados sobre as comunicações de autoridades e cidadãos no Brasil e no mundo.**

**Assim, a obrigação de guardar registros de aplicação foi introduzida nas últimas fases do debate e foi bastante criticada, tanto por representantes da sociedade civil por enfraquecer a proteção à privacidade dos usuários, como pelo setor privado, por gerar custos e responsabilidades adicionais relacionados ao armazenamento e a segurança de tal guarda.** [VARON-MCI-2014]_

****

Críticas que se apoiaram também no fato de que, em momento semelhante, mas sentido contrário, a Diretiva 24EC/2006 da União Européia, denominada de Diretiva de Retenção de Dados, foi julgada inconstitucional pela Corte Europeia em abril de 2014 [VARONeCASTANHEIRA-24EC-2014]_.

A análise “Marco Civil da Internet: seis meses depois, em que pé que estamos?” também retrata a polêmica da inclusão do artigo de retenção de dados:

     [...] Regimes de manutenção coletiva de dados, sem a necessidade de
     suspeita de ato malicioso, correompem as pré-condições a uma
     sociedade aberta e democrática, por enfraquecerem a confiança
     depositada pelos indivíduos na privacidade de suas comunicações e
     por criarem um risco permanente de perda e violação de dados.
     [ARTIGO19-MCI-2015]_

O PL215/2015, que encontra-se `tramitando <https://antivigilancia.org/pt/2015/11/plespiao-segue-no-congresso/>` na Câmara dos Deputados, se propõe a
fazer justamente isso.

O fato é que, a cada dia, nossos computadores, celulares e uma gama de outros dispositivos coletam e processam mais e mais dados pessoais sensíveis – aqueles que revelam a “origem racial ou étnica, as convicções religiosas, filosóficas ou morais, as opiniões políticas, a filiação a sindicatos ou organizações de caráter religioso, filosófico ou político, dados referentes à saúde ou à vida sexual, bem como dados genéticos”, como define o `Anteprojeto de Proteção de Dados Pessoais <http://pensando.mj.gov.br/dadospessoais/>`_ do Ministério da Justiça.

Tais dispositivos também estão cada vez mais interconectados através de serviços como e-mail, redes sociais, aplicativos de mensagens e computação em nuvem, por onde todos estes dados (fotos, mensagens, documentos, geolocalização, buscas, e até passos dados durante o dia) são transmitidos e encaminhados para seus destinatários. Neste cenário, a retenção de dados se torna cada vez mais custosa.

Para definir o direito à privacidade neste novo contexto, o Conselho de Direitos Humanos das Nações Unidas publicou o relatório “The Right to Privacy in the Digital Age” (“privacidade na era digital”). Nele, é afirmado que a retenção de dados interfere na privacidade até mesmo quando os dados nunca são usados – no caso, se referindo aos programas de vigilância em massa da agência de segurança nacional dos EUA, a NSA (tradução e grifo nosso):

    Segue disso que qualquer captura de dados de comunicação é
    potencialmente uma interferência na privacidade e, além disso, que **a
    coleta e retenção de dados de comunicações significa uma
    interferência com a privacidade quer ou não estes dados sejam
    posteriormente consultados ou usados**. Mesmo a mera possibilidade das
    informações de comunicação serem capturadas cria uma interferência
    com a privacidade, com um efeito desencorajador (*chilling effect*) em
    direitos, incluindo aqueles à liberdade de expressão e associação. A
    própria existência de um programa de vigilância em massa então cria
    uma interferência com a privacidade. [UN-PRIVACY-2014]_

O professor e pesquisador Daniel J. Solove, “um dos maiores especialistas em leis de privacidade” segundo o fórum de experts em TI SafeGov, descreve como o *chilling effect* causa danos à liberdade de expressão e de associação e à democracia (tradução nossa):

    Até a vigilância de atividades legais pode inibir as pessoas de
    engajarem-se nelas. O valor da proteção contra *chilling effects* não
    é medido simplesmente focando nos indivíduos em particular que foram
    impedidos de exercitar seus direitos. Os *chilling effects* causam
    danos à sociedade porque, entre outras coisas, reduzem a variedade
    de pontos de vista expressados e o grau de liberdade de se engajar
    em atividades políticas. [SOLOVE-PRIVACY-2008]_

Mas a falta de sincronia com a tendência européia, internacionalmente conhecida com mais protetiva do exercício do direito à privacidade, bem como esse *chilling effect* na liberdade de expressào não são os únicos elementos de crítica. Abaixo destacamos alguns outros aspectos e soluções possíveis a serem consideradas também no mandato da CPI Ciber:


.. _custos:

Custos
======

**Reter dados em todos os provedores de conexão e aplicação comerciais
também cria muitos custos novos: sistemas de armazenamento e backup,
funcionários(as) para manter os sistemas e atender às demandas judiciais,
e a segurança física e digital dos registros, que carregam em si grande
valor comercial e muitas vezes poder político ou econômico.**

**Estes custos, além de prejudicarem o mercado como um todo, afetam
especialmente as pequenas empresas e as startups, que devem arcar com as
despesas de uma infraestrutura de armazenamento antes mesmo de terem um
mercado consolidado.**

****

Para armazenar uma quantidade cada vez maior de dados (e *backups* deles), os
provedores precisam arcar com os custos de operação e manutenção de
*datacenters*. Após o governo australiano passar uma lei de retenção de dados
que obriga os provedores a armazenar os metadados de clientes por no mínimo 2
anos, os custos estimados pela indústria para a adaptação do setor de
telecomunicações se encontram entre 300 e 700 milhões de dólares australianos
[KNOTT-WROE-2015]_. Este cálculo é feito em cima de 12,5 milhões de
assinaturas de Internet na Austrália; de acordo com pesquisa do NIC.br e
Cetic.br, em 2014 haviam mais de 32 milhões de domicílios com acesso à
Internet no Brasil [TICDOMICILIOS-2015]_.

.. _vazamentos:

Vazamentos
==========

**Mesmo seguindo padrões e boas práticas de segurança, nenhum sistema é
completamente seguro e o risco de invasões e vazamentos é sempre
presente.**

****

O vazamento de dados através de invasões, erros técnicos, corrupção de
funcionários e outros fatores têm atingido até mesmo em gigantes do setor
-- como Sony, Target, Adobe, Ebay, Vodafone, SnapChat, Twitter, Facebook,
Steam, Blizzard e dezenas de outras presentes `numa compilação de grandes
vazamentos de bancos de dados
<http://www.informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/>`_ dos últimos anos.

A natureza da segurança digital faz com que vulnerabilidades novas para plataformas antes consideradas seguras surjam o tempo inteiro; com trabalho especializado, custos de serviços e equipamento,e uma série de práticas e rotinas de segurança, é possível proteger um sistema da maior parte das ameaças; mesmo o trabalho dedicado de uma equipe, no entanto, pode ser contornado por um adversário com determinação e recursos suficientes.

Se o problema é alarmante até mesmo para organizações estabelecidas e cautelosas, é mais ainda para provedores em todo o país e no exterior que têm, em média, muito menos recursos e incentivos para proteger seus sistemas. Obrigar a guarda de registros aumenta o grau e o alcance dos danos que vazamentos podem causar aos provedores e seus clientes.

Também devem ser contabilizados nos custos da retenção de dados aqueles associados aos vazamentos, como os gastos jurídicos de retratação, o
esforço e a inconveniência para um indivíduo de ter seus dados expostos
(frequentemente com danos econômicos e morais, e por vezes até psicológicos ou fatais) e do impacto social da insegurança geral das interações *online*.

.. _metadados:

Metadados
=========

**Os metadados também são protegidos pelo sigilo das comunicações, por
serem tão sensíveis quanto o conteúdo, e por vezes mais fáceis de serem
usados para revelar informações e padrões de comportamento de indivíduos
e organizações.**

****


O termo *metadado*, "dado sobre outros dados", significa tudo que é trafegado
pela rede ou armazenado em disco que não seja o *conteúdo* em si de uma mensagem ou arquivo
(como o corpo de um e-mail ou os *pixels* de uma imagem), e sim
informações de roteamento, categorização ou descrição.

No nível da rede TCP/IP, os roteadores que compõe a Internet precisam saber de
onde vem e para onde vai um determinado pacote para poder encaminhá-lo de forma
correta. Os endereços IP de origem e destino são então os *metadados* dos
pacotes.

Já no nível de serviços, tomando o e-mail como exemplo, os endereços de origem
e destino de uma mensagem (como ``fulana@camara.gov.br`` e ``beltrano@exemplo.adv.br``), a data e hora de
envio e o assunto ("Preciso de ajuda"), são alguns de seus *metadados*.

.. image:: imagens/metadados_email.png
    :align: center

A carta Princípios Internacionais sobre a Aplicação Dos Direitos Humanos na Vigilância Das Comunicações, o "resultado de uma consulta global com grupos da sociedade civil, da indústria e especialistas internacionais em questões jurídicas, políticas e tecnológicas relacionadas à Vigilância das Comunicações", explica o poder dos metadados:

    Os metadados de comunicações podem criar um perfil de vida do indivíduo,
    incluindo questões médicas, pontos de vista políticos e religiosos,
    associações, interações e interesses, revelando tantos detalhes quanto — ou
    ainda mais — do que seria perceptível a partir do conteúdo das comunicações.
    Apesar do vasto potencial de intromissão na vida do indivíduo e do efeito
    desencorajador ("*chilling effect*") sobre a associação política e de outra
    natureza, as leis, regulamentos, atividades, poderes ou autoridades
    frequentemente atribuem aos metadados de comunicações um nível de proteção
    menor e não impõem restrições suficientes a como eles podem ser usados
    posteriormente pelos Estados. [13-PRINCIPIOS-2014]_

Dennys Antonialli, diretor executivo do InternetLab,
`exemplificou <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos/documentos/audiencias-publicas/audiencia-publica-dia-12-11.15/apresentacao-dennys-antonialli>`_
muito bem tal poder em audiência pública dessa CPI ao mostrar a "rede
social" que é possível extrair através somente dos endereços de origem e
destino e da data/hora de seus e-mails através da ferramenta Immersion,
desenvolvida no Massachusets Technology Institute (MIT):

.. figure:: imagens/metadados.png

    Rede de contatos de Dennys. Cada cor representa um círculo social
    diferente (por exemplo, "família" e "colegas de trabalho"); o tamanho do
    círculo é a quantidade de mensagens trocadas e as ligações entre os
    círculos são feitas quando as pessoas estão em cópia numa mesma
    mensagem.

A Corte Interamericana de Direitos Humanos (CIDH) explicitou, no caso *Escher e outros vs. Brasil* que os metadados também são abarcados na proteção à privacidade:

    [O direito à privacidade] aplica-se às conversas telefônicas
    independentemente do conteúdo destas, inclusive, pode compreender tanto
    as operações técnicas dirigidas a registrar esse conteúdo, mediante sua
    gravação e escuta, como qualquer outro elemento do processo
    comunicativo, como, por exemplo, o destino das chamadas que saem ou a
    origem daquelas que ingressam; a identidade dos interlocutores; a
    frequência, hora e duração das chamadas; ou aspectos que podem ser
    constatados sem necessidade de registrar o conteúdo da chamada através
    da gravação das conversas. Finalmente, a proteção à vida privada se
    concretiza com o direito a que sujeitos distintos dos interlocutores
    não conheçam ilicitamente o conteúdo das conversas telefônicas ou de
    outros aspectos, como os já elencados, próprios do processo de
    comunicação [CIDH-2009]_


.. _minima:

Retenção mínima
===============

**A melhor medida de segurança contra o vazamento de dados é não guardá-los. Uma política de retenção mínima de dados, onde os sistemas são desenhados para coletar o mínimo de informações necessárias para seu objetivo, fortalece a privacidade, a liberdade de expressão e a segurança de toda a sociedade ao remover pontos de acúmulo de dados, que por seu valor econômico e político são visados por criminosos, espiões e agências de inteligência de diversos países.**

****

Para maior proteção de direitos fundamentais como a privacidade e a liberdade de expressão, e também para maior segurança dos usuários contra vazamentos de dados, a política de reter o mínimo de dados para o funcionamento do serviço é ideal. Como explicou Claudia Melo para o Technology Radar, um estudo da empresa de software ThoughtWorks:

    Diversos setores do mercado brasileiro vêm considerando Big Data uma das
    apostas para alavancar o negócio e melhorar o relacionamento com os
    clientes. Se por um lado a tecnologia permite que todos os seus dados,
    interesses e interações sejam armazenados e analisados, por outro há sérios
    riscos relacionados à privacidade das pessoas", afirma Claudia Melo,
    Diretora de Tecnologia da ThoughtWorks Brasil. "Nós defendemos que as
    empresas devem armazenar somente o mínimo necessário de informações de seus
    clientes, uma política que alemães denominam de *datensparsamkeit*", afirma
    Martin Fowler, cientista chefe da ThoughtWorks. [THOUGHTWORKS-BIGDATA-2014]_

O conceito alemão de **datensparsamkeit** está em sintonia com o que Cristiana
Gonzalez, pesquisadora do Instituto de Defesa do Consumidor,
`manifestou <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos/documentos/audiencias-publicas/audiencia-publica-dia-22-09-15/cristiana-de-oliveira-gonzalez-instituto-brasileiro-de-defesa-do-consumidor-idec>`_
em
audiência pública dessa CPI: "Sistemas deveriam ser desenhados para terem o
mínimo de vigilância necessário e coletar o mínimo de informação necessária
para determinado objetivo".
