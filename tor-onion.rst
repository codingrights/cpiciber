.. _tor:

================
Tor e Rede Onion
================

Talvez o ponto de maior preocupação mas também menor familiaridade da
CPICIBER seja o uso do Tor. Aqui, apresentamos uma breve história do Tor
e de como ele funciona (tanto para pessoas não serem associadas à sua
navegação quanto para hospedar sites e serviços que não podem ser
localizados), as ocorrências e tentativas passadas de quebrar o
anonimato que a rede Tor provê, e a nossa posição sobre que métodos
devem ser utilizados para investigar crimes na chamada "dark web", que
preferimos chamar de **onion web**. Esta rede também frequentemente é
chamada de *deep web*, um termo que já possui outro significado e que
explicamos melhor na seção "Dark Web? Deep Web?" abaixo.

Tor
===

O Tor – nome derivado do antigo acrônimo "The Onion Routing", "O
Roteamento Cebola" – é o nome tanto de um software mantido por uma
organização sem fins lucrativos sediada em Massachussetts, EUA, quanto
da rede mundial de *relays* ("retransmissores"), computadores mantidos por
pessoas e organizações voluntárias.

.. figure:: imagens/tor-globo.png
    :align: center

.. figure:: imagens/tor-relays.png
    :align: center

    Imagens traduzidas do guia "How Tor Works?" de Jordan Wright [WRIGHT-TOR-2015]_. Licença: CC BY 3.0 <https://creativecommons.org/licenses/by/3.0/>.

Como funciona
-------------

Quando alguém usa o Tor para acessar um site ou ler e-mails, seu computador
escolhe três desses relays para encaminharem seu tráfego de forma que ele saia
para a Internet com o IP do último deles, o *relay* de saída.

Como há muitos *relays* na rede Tor (cerca de 7200 em 15 de fevereiro de
2016), e pessoas utilizando o serviço em todo o mundo (mais de 2 milhões
na mesma data), usá-la tem o efeito de anonimizar a sua conexão,
impedindo que os seus pacotes de dados trafegados sejam ligados ao seu
endereço IP tanto pelo provedor de conexão quanto pelo servidor
acessado.

Além disso, quando o computador vai enviar uma mensagem ou acessar uma
página através da rede Tor, o conteúdo é enviado com três camadas de
criptografia, uma para cada *relay*. Isto e mais algumas técnicas
matemáticas garantem que nem mesmo os *relays* que participam da rede
possam saber o que estão encaminhando e ao mesmo tempo quem é o
remetente.

.. figure:: imagens/tor-camadas.png
    :align: center

    Imagem traduzida do guia "How Tor Works?" de Jordan Wright [WRIGHT-TOR-2015]_. Licença: CC BY 3.0 <https://creativecommons.org/licenses/by/3.0/>.

O *relay* guarda, primeiro da fila, pode saber que determinado IP usou a
rede Tor, mas não sabe para quê. O *relay* intermediário não tem acesso
nem ao conteúdo, nem ao remetente, nem ao destinatário. O *relay* de saída
finalmente encaminha o pacote para a "Internet aberta", então deve saber
o destinatário e o conteúdo, mas não o remetente. Se há uso de criptografia
entre remetente e destinatário (o que grande parte dos serviços na Internet têm
empregado), o *relay* de saída também não consegue saber o conteúdo do acesso
ou da comunicação.

.. seealso::
    :ref:`Criptografia <criptografia>`

A tecnologia principal por trás do Tor, o "roteamento cebola" (nome dado devido
às camadas de criptografia sobrepostas), tem suas origens no Laboratório de
Pesquisa Naval dos EUA. Paul Syverson, um matemático da equipe, afirmou
em 2011 que a ideia "não é prover comunicações anônimas, mas sim separar
a identificação do roteamento". Através de técnicas de criptografia, e
tendo pessoas diversas o suficiente e espalhadas pelo mundo dispostas a
manter *relays*, a Rede Tor funciona como uma comunidade voluntária que
acredita que "o uso de uma rede pública não deve automaticamente revelar
as identidades das partes que se comunicam" [SYVERSON-ONION-2011]_. Para Jacob Appelbaum, pesquisador de segurança, desenvolvedor e porta-voz do Projeto Tor, a rede tem o objetivo de "instrumentalizar a liberdade de conexão", um dos princípios da Internet.

Após o laboratório tornar o código do software público em 2004 e ser
fundada a ONG Tor Project para mantê-lo de forma independente em 2006,
o tamanho da rede de *relays* cresceu bastante, tanto através de
voluntários(as) individuais quanto de variadas entidades que defendem a
liberdade de expressão: organizações sem fins lucrativos como as
americanas Mozilla e Access Now e a alemã Torservers.net; universidades
como a de Galileo, na Guatemala, e as de Michigan e Pennsylvania nos
EUA; a biblioteca Lebanon, em New Hampshire, EUA, e até galerias que abrigam o
projeto artístico Autonomy Cube, como a Casa Edith Russ na Alemanha.

.. figure:: imagens/tor-mapa-mundo.png
    :align: center

    Visão geral dos *relays* Tor ao redor do mundo, agrupado por regiões de proximidade. Imagem retirada do site ``map.torservers.net``, em dezembro de 2015.

.. figure:: imagens/tor_team.png
    :align: center

    Parte dos membros pagos e voluntários do Projeto Tor, em um vídeo de agradecimento ao agregador social de notícias Reddit, que dividiu 10% de seu lucro em doações para projetos e organizações votados pelos(as) usuários(as). O Projeto Tor foi um deles, e recebeu US$ 82 mil.

\
O Tor é uma ferramenta muito importante para uma variedade de profissões
e atividades ao proteger as comunicações e a navegação online. Tanto
jornalistas investigativos quanto policiais e agentes de inteligência
usam o Navegador Tor para conduzir suas investigações sem alertar os
sites investigados sobre suas visitas; ativistas e defensores de
direitos humanos usam a ferramenta para se organizar e discutir temas
controversos sem risco de represálias, prisão ou morte em regimes
não-democráticos (como no Irã, na China e na Síria). Através de
ferramentas de chat que usam a rede Tor como o Tor Messenger e o Ricochet,
jornalistas e advogados podem conversar de forma segura com suas fontes
/ clientes sem gerar trilhas de informação que possam levar terceiros à
identidade ou à localização dos últimos [TEIXEIRA-CHATANONIMO-2015]_.


Milhões de cidadãos e cidadãs "comuns" usam o Tor para navegar pela Web
sem serem monitorados por trackers, que usam o endereço IP, cookies e
outras tecnologias de rastreamento para enxergar perfis de comportamento
e consumo e gerar propagandas direcionadas [TEIXEIRA-TRACKERS-2015]_, escapar
de sistemas de vigilância em massa conduzida por agências de inteligência
ou buscar informações e cuidado em temas pessoais e sensíveis como
problemas de saúde, alcoolismo, traumas por exploração sexual ou
psicológica, etc.


À prova de censura: o Tor é feito para não poder ser bloqueado
--------------------------------------------------------------

O Tor também é usado para evadir barreiras de censura, como o bloqueio de
redes sociais, serviços de comunicação e sites políticos em países como o
Irã e a China, que desde 2009 bloqueia o acesso aos *relays* da rede Tor
em seus provedores (os endereços IP dos *relays* são públicos). A
resposta a isso foi o desenvolvimento das *bridges* ("pontes"), *relays*
guarda que não são listados publicamente e que o projeto Tor distribui de
maneira privada, em pequenas quantidades, através de e-mail.

O consequente emprego de técnicas de *deep packet inspection* ("inspeção
profunda de pacotes") para identificar os pacotes de conexão com as
bridges e bloqueá-los  foi contornado com a criação dos
*pluggable transports*, ferramentas que camuflam o tráfego entre a pessoa
e a bridge para que se pareça com o de outra aplicação (por exemplo, uma
ligação via Skype ou o envio de um e-mail).

(cabe lembrar que tal prática, extremamente intrusiva, no Brasil
violaria o Marco Civil da Internet tanto em seu art. 7º, inciso II,
relativo à inviolabilidade das comunicações, quanto em seu artigo 9º,
relativo à neutralidade da rede, e também a Lei Geral das
Telecomunicações em seu art. 3º, que garante a inviolabilidade e o
segredo das comunicações)

Rede Onion e Onion Web
======================


.. image:: imagens/tor.png
    :align: center

Até agora, vimos como o Tor permite às pessoas acessar conteúdos e se
comunicar na Internet sem que suas atividades deixem registros que
possam lhes identificar. Outra funcionalidade do Tor é permitir a
criação de "serviços escondidos" (hidden services), ou "serviços *onion* /
cebola" (onion services). Eles formam o que é conhecido como "rede
onion", "dark web", ou (erroneamente, como :ref:`veremos <deepweb>`) "deep web".

Quando se opera um site na *onion web*, ele passa a ser acessível
através da rede Tor com um endereço especial, como
``as2xfiuknfagm53d.onion``. Ele permite à rede de *relays* estabelecer um
canal de comunicação onde tanto o computador cliente (quem acessa)
quanto o computador servidor (o site acessado) desconhecem o endereço IP
ou qualquer outro dado que possa ser associado à localização ou à
identidade das pessoas que os operam.

O objetivo dos serviços *onion*, como `publicado
<https://blog.torproject.org/blog/crowdfunding-future-hidden-services>`_ no
blog do projeto, é  "prover uma maneira de usuários(as) do Tor criarem sites e
serviços acessíveis exclusivamente dentro da rede Tor, com características de
privacidade e segurança que os tornam úteis e atraentes para uma ampla
variedade de aplicações".

Estas "características de privacidade e segurança" de fato vão muito
além de esconder a localização do servidor – segundo Roger Dingledine,
fundador do Projeto Tor e idealizador dos serviços *onion*, eles provêem
autoautenticação (isto é, o próprio endereço do site pode ser usado para
autenticá-lo como verdadeiro e não um site falso se passando por ele),
criptografia ponta-a-ponta ou fim-a-fim (isto é, a garantia da
confidencialidade das comunicações entre as duas partes), *NAT punching*
(uma forma de hospedar sites e serviços em redes compartilhadas ou atrás
de *firewalls*) e área de superfície delimitada (um modo simples de
expôr somente as portas necessárias do servidor, desvinculadas de seu
endereço IP).

Em outubro de 2014, o Facebook `passou a manter um serviço *onion*
<https://www.facebook.com/notes/protect-the-graph/making-connections-to-facebook-more-secure/1526085754298237>`_,
no endereço ``facebookcorewwwi.onion``, como forma de tornar as conexões
via Tor de seus usuários mais seguras e eficientes. Isto foi feito não
para esconder sua localização – a Facebook Inc. é uma empresa formalmente
estabelecida, e os locais de seus *datacenters* conhecidos – mas para ser
acessível a pessoas em países onde a rede social é bloqueada, como na
China e na Turquia, e por usuários que se sentem confortáveis em ceder ao
Facebook suas informações sociais, mas não seu endereço IP ou
localização.

.. _garantia-de-anonimato:

Uma garantia técnica do anonimato
---------------------------------

Operar um serviço *onion* é o meio técnico mais confiável de garantir de
fato a promessa de anonimato em uma denúncia – projetos como o
`GlobaLeaks <https://globaleaks.org/>`_ e o `SecureDrop <https://securedrop.org/>`_ fornecem software livre e gratuito para criar
canais de denúncia anônima na imprensa e em empresas. Por serem
acessíveis somente através da rede Tor, eles colhem as informações de
denúncia sem deixar rastros não-intencionais que possam levar à/ao
denunciante; como não é necessário encaminhar o tráfego para a Internet
"aberta", todo o caminho entre denunciante e servidor é protegido de
espionagem ou censura.

Algumas organizações que usam esta tecnologia são os jornais Washington
Post, Guardian e New Yorker; a revista Forbes; e a ONG Greenpeace.
Outros canais de denúncia surgiram focados especificamente no combate à
corrução, como o Allerta Anticorruzione na Itália, o OCCRPLeaks na
Bósnia, o Brussel Leaks na Bélgica, o Xabardocs na Ucrânia e o
InfodioLeaks na Venezuela.

Já o `WildLeaks <https://wildleaks.org/>`_ usa a rede Tor para receber denúncias
relacionadas "à vida selvagem e aos delitos florestais". Em vez de encarar
diretamente criminosos armados, o projeto da ONG Elephant Action League "quer
mirar os maiores traficantes de chifres de rinocerontes e presas de elefantes,
que lucram milhões de dólares com sua atividade", segundo matéria da National
Geographic Brasil [NATGEOBR-WILDLEAKS-2014]_. Algumas das 24 ocorrências
recebidas durante os três primeiros meses de operação, em 2014, foram denúncias
"de caça a tigres no norte de Sumatra, de contrabando de macacos, em particular
chimpanzés, na África Central, atividades madeireiras ilegais no México, Malawi
e Rússia, [e] pesca ilegal na costa do Alasca."

.. seealso::
    :ref:`Anonimato Online > Usos legais do anonimato no Brasil > Denúncias anônimas <denuncias>`

.. _liberdade_de_conexao:

A censura à rede fere a liberdade de conexão
--------------------------------------------

O que torna o bloqueio ou a censura automática de conteúdo (como pornografia infantil e crimes de ódio) incompatível com o princípio da *liberdade de conexão*, embutido nos protocolos da Internet e materializado aqui pelo Tor, é que qualquer mecanismo técnico que possibilite tal censura será inevitavelmente explorado por indivíduos ou organizações com fins não tão nobres quanto o da proteção das pessoas e a investigação de crimes. Como disse Jacob Appelbaum, pesquisador de segurança e membro do Projeto Tor, ao responder um oficial da polícia alemã "se era possível bloquear determinados conteúdo abusivos da *onion web*", num `painel do evento re:publica 2015 <https://re-publica.de/session/deeper-frontier-freedom-state-deepweb>`_:

    Nós não podemos censurar este conteúdo [pornografia infantil e venda de
    armas] porque a natureza da liberdade de conexão significa que se pusermos
    uma pessoa no meio para policiar o conteúdo, vários tipos diferentes de
    conteúdo ficarão fundamentalmente em risco; por exemplo há esta
    questão bastante importante e séria -- eu levo bem a sério. O genocídio
    tibetano, perpretado pelo governo Chinês, é um assunto que tal governo
    gostaria de censurar, para eles é um assunto muito tabu. Se nós embutirmos
    a censura tecnologicamente nos nossos sistemas, as máquinas simplesmente
    farão valer a censura indiscriminadamente para quem quer que controle essas
    tecnologias,  [...] como você sabe que aconteceu com a lista de censura
    aqui [na Alemanha], e de fato com vários exemplos históricos de vigilância
    e censura. Fundamentalmente, a solução não é mais vigilância e mais
    censura, e sim enfrentar o núcleo, a raiz do problema.

.. seealso::
    :ref:`Questões Emergentes > Deep Web <deepweb>`


Quebras da Rede Onion em investigações policiais
=================================================

Dentre as operações policiais passadas a usuários(as) do Tor e
operadores(as) de sites na *onion web* ao redor do mundo, relativamente poucas
foram através de ataques técnicos. Apresentamos aqui alguns detalhes sobre os
métodos mais relevantes – em um caso, uma falha no funcionamento interno da
rede Tor; no outro, a exploração de falhas de segurança nos servidores dos
sites. Então discutimos os limites e os riscos desse tipo de abordagem, e
apresentamos outras mais saudáveis para a manutenção das liberdades na
Internet, da privacidade e dos direitos humanos, e também mais dentro da
realidade técnica e financeira da grande maioria das investigações.

Durante a investigação conduzida pelo FBI sobre o Silk Road 2 (um
mercado onde vendedores(as) podiam anunciar, se comunciar com clientes e
fazer as transações sob um pseudônimo, bastante conhecido pela oferta
de narcóticos e substâncias psicoativas), foi citada uma infiltração na
rede *onion* que permitia descobrir o
endereço IP verdadeiro de um site disponível nesta rede. O Projeto Tor
atualizou o software para que não permitisse mais tal ataque em julho de
2014, ao `descobrir e anunciar
<https://blog.torproject.org/blog/tor-security-advisory-relay-early-traffic-confirmation-attack>`_
a atividade anômala na rede, que perdurou por seis meses. Através de um
novo documento anexado a um dos processos judiciais do Silk Road, foi
descoberto que o FBI foi ajudado por um "instituto de pesquisa sediado em uma
universidade". 

O período de duração da vulnerabilidade e outras evidências apontam para
o trabalho de um time de pesquisadores da universidade americana
Carnegie Mellon [WIRED-SR2-2015]_. Roger Dingledine, fundador do Projeto
Tor, afirmou que a universidade recebeu 1 milhão de dólares para conduzir
a pesquisa [OLHARDIGITAL-TORCMU-2015]_.

Outro modo utilizado para identificar o verdadeiro IP dos sites na rede
*onion*, em operações policiais ou ações judiciais, é a invasão direta do
servidor – aproveitando-se de erros de configuração de quem opera o
site, ou vulnerabilidades nos softwares utilizados por ele. Através
destas técnicas – combinadas com a investigação "tradicional", sem
quebras de sigilo, online e offline – o FBI conduziu a operação que
prendeu Ross Ulbricht, acusado de operar o "primeiro" Silk Road.

Utilizar vulnerabilidades de segurança, no entanto, tem um custo alto e
traz uma série de responsabilidades e riscos.

.. seealso::
    :ref:`Questões Emergentes > Invasão de computadores <invasao>`

Os métodos da Operação Darknet, que prendeu pelo menos 55 pessoas no
Brasil envolvidas com troca de imagens de pornografia infantil na rede
*onion*, são desconhecidos. O coordenador da operação, Rafael França,
diz para a VICE News que sua equipe conseguiu identificar dezenas de
usuários(as) da rede que compartilhavam pornografia infantil, utilizando
"ferramentas desenvolvidas e metodologias inéditas de pesquisa"
[VICE-DARKNET-2014]_.

É possível que tenha havido uma colaboração com o FBI – as prisões da
operação brasileira foram feitas três meses após o Projeto Tor consertar
a brecha que permitia o ataque de identificação usado pela agência, mas
a vulnerabilidade era voltada a identificar os servidores dos sites
(como um fórum de troca de imagens) e não quem os acessava (seus
endereços IP permaneceriam ocultos) [BRASIL-DARKNET-2014]_.

O uso de tal método também desperta algumas questões idênticas às do seu
uso no Silk Road 2: ele não pode ser direcionado a um site específico,
envolvendo necessariamente a quebra da proteção das centenas ou milhares
de sites e serviços legítimos operando na rede. Se esse tipo de
abordagem desperta uma série de dúvidas até em situações extremas,
certamente não deve ser a abordagem corriqueira para investigar crimes
na *onion web*.

Outros métodos mais "tradicionais" também foram usados com sucesso
na Operação Darknet, como mapear websites em espaços e meios públicos
("open source intelligence") e infiltrar agentes. O coordenador Rafael
França explica, na mesma matéria da VICE News: "Nós descobrimos e
fizemos contato com comunidades para pessoas interessadas em pornografia
infantil, e gradualmente começamos a nos comunicar com elas".

Apesar da localização dos websites não poder ser descoberta quando ele
opera dentro da rede Tor, ainda é necessário tornar seus endereços de
acesso públicos para potenciais clientes, então um mínimo de exposição é
necessário: seja em um site que os catalogue manualmente como a Hidden
Wiki, (um tipo de Wikipédia para sites conhecidos na *onion web*)
através de fóruns na Web aberta, ou outros meios.

É possível então analisar estes meios sistematicamente para criar listas de
websites e analisar fóruns e canais públicos como o Reddit e o Pastebin, para
criar buscadores análogos ao Google e Bing e conduzir análises específicas de
combate a determinados crimes.


.. figure:: imagens/blackhat-deepweb.png
    :align: center

    Os pesquisadores Marco Balduzzi e Vincenzo Ciancaglini apresentaram a
    pesquisa em ferramentas de investigação para a Deep Web que desenvolveram através de análise de dados disponíveis publicamente, sem quebras de sigilo, na apresentação `Cybercrime in the Deep Web <https://www.youtube.com/watch?v=OcuzaOLs7dM>`_.
