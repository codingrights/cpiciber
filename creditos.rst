========
Créditos
========

.. only:: latex

    .. figure:: imagens/logo_codingrights.png
        :alt: Coding Rights
        :target: https://codingrights.org/

.. only:: latex

    .. figure:: imagens/logo_beta.png
        :alt: Instituto Beta para Internet e Democracia
        :target: http://ibidem.org.br/

.. only:: latex

    .. figure:: imagens/logo_intervozes.png
        :alt: Intervozes
        :target: http://intervozes.org.br/

Redação
=======

.. only:: html

    .. figure:: imagens/logo_codingrights.png
        :alt: Coding Rights
        :target: https://codingrights.org/

****

**Lucas Teixeira** é desenvolvedor, administrador de sistemas e
pesquisador. Atua como diretor técnico da `Coding Rights <https://codingrights.org/>`_, sendo editor do
Boletim Antivigilância e um dos criadores do projeto Oficina
Antivigilância. Guiado pelo espírito comunitário, ele tem experiência de
trabalho em projetos colaborativos e voluntários sobre privacidade, liberdade
de expressão, software livre, agroecologia e cultura livre. Também tem
desenvolvido métodos de oficinas para treinamento em segurança digital,
produzido e traduzido guias e plataformas sobre o tópico.

**Joana Varon** é fundadora e diretora da `Coding Rights <https://codingrights.org/>`_, advogada e bacharel em
Relações Internacionais pela PUC-SP, mestre em Direito e Desenvolvimento pela
Escola de Direito de São Paulo da Fundação Getúlio Vargas (FGV-SP) e
especializada em direito e novas tecnologias, já atuou como pesquisadora no
Cebrap-SP, no Observatório de Inovação e Competitividade da USP e no Centro de
Tecnologia e Sociedade da FGV. Nessa trajetória tem trabalhado em pesquisa
aplicada para políticas públicas na área de TICS, como enfoque em análises
juridico-institucionais para garantir direitos humanos e inovação na era
digital.

.. only:: html

    .. figure:: imagens/logo_beta.png
        :alt: Instituto Beta para Internet e Democracia
        :target: http://ibidem.org.br/

****

**Paulo Rená** é mestre em Direito, Estado e Constituição (UnB). Professor de
Direito (UniCEUB). Chefe Executivo de Pesquisa do `Instituto Beta Para Internet
e Democracia - IBIDEM <http://ibidem.org.br/>`_. Atuou no Ministério da Justiça (SAL/MJ) como gestor do
projeto de elaboração coletiva do Marco Civil da Internet no Brasil.

Apoio
=====

.. only:: html

    .. figure:: imagens/logo_intervozes.png
        :alt: Intervozes
        :target: http://intervozes.org.br/

****

**Bia Barbosa** é jornalista, integrante da Coordenação Executiva do `Intervozes <http://intervozes.org.br/>`_ e do `Fórum Nacional pela Democratização da Comunicação <http://www.fndc.org.br/>`_.


Agradecimentos
==============

Para as ilustrações, foram usados ícones do `Projeto GNOME <https://gnome.org/>`_ e *cliparts* do `Openclipart <https://openclipart.org/>`_ e dos `Iconoclasistas <http://www.iconoclasistas.net/>`_.

Gostaríamos de agradecer, por inspiração e colaboração, a Guilherme Damasio Goulart e Vinicius Serafim (do `podcast Segurança Legal <http://www.segurancalegal.com/>`_), Gustavo Paiva e o `Grupo de Trabalho de Anonimato no Brasil <https://lists.riseup.net/www/info/gt_aib>`_, e a Comunidade `Antivigilância <https://antivigilancia.org/>`_.

