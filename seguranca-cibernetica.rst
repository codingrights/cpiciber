.. _segciber:

=====================
Segurança Cibernética
=====================

Definição
=========

O conceito de **segurança cibernética** é difuso; assim como conceitos semelhantes de 'segurança da informação', 'ciberguerra' e 'cibervigilância', eles "não foram acordados através de um órgão internacional mediante um acordo vinculante, ou emitindo um documento que determine padrões". [ROSSINI-CIBERSEG-2015]_

De acordo com a UIT / ITU (União Internacional das Comunicações, um órgão da ONU), a segurança cibernética é "a coleção de ferramentas, políticas, conceitos de segurança, salvaguardas de segurança, orientações, abordagens de gestão de risco, ações, treinamentos, melhores práticas, seguros e tecnologias que podem ser usados para proteger o ambiente cibernético, a organização e propriedades de usuários(as). A organização e as propriedades incluem dispositivos de computação conectados, funcionários(as) e colaboradores(as), infraestrutura, aplicativos, serviços, sistemas de telecomunicações e a totalidade de informação transmitida e/ou armazenada no ambiente cibernético. A segurança cibernética busca garantir a obtenção e a manutenção das propriedades de segurança da organização e das propriedades do(as) usuários(as) contra riscos de segurança relevantes no ambiente cibernético" [ITU-CIBERSEG-2008]_

A União Europeia define o conceito como o conjunto de "salvaguardas e ações que se podem empregar para proteger o domínio cibernético, tanto no âmbito civil quanto militar, frente às ameaças vinculadas com suas redes interdependentes e sua infraestrutura de informação, ou que possam afetar a estas". [EC-CIBERSEG-2013]_

Já o governo brasileiro, através do Gabinete de Segurança Institucional da
Presidência da República (GSI/PR), define a segurança cibernética como "a arte de
assegurar a existência e a continuidade da Sociedade da Informação de uma
Nação, garantindo e protegendo, no Espaço Cibernético, seus ativos de
informação e suas infraestruturas críticas" [BRASIL-CIBERSEG-2015]_.

Abordagens ruins da segurança cibernética e de como se proteger de ameaças digitais
podem gerar tanto sistemas de vigilância em massa -- como os documentos vazados
por Edward Snowden revelaram ser o caso por parte de agências de intleigência
dos EUA, da Inglaterra e de países aliados e, pode-se argumentar, também o da
:ref:`retenção de dados do Marco Civil da Internet <retencao_violacao>` --
quanto por censura e perseguição de pesquisadores de segurança (como
`na China <https://www.eff.org/deeplinks/2016/01/china-shows-how-backdoors-lead-software-censorship>`_)
e uma excessiva aparelhagem ofensiva de "guerra cibernética" (outro conceito
difuso).

Como escrevem Andrew Puddephatt e Lea Kaspar, *experts* em política e governança da Internet da Global Partners Digital, para a openDemocracy:

    Entre a sociedade civil e grupos de interesse público, no entanto, ainda há
    no momento pouco engajamento ou mesmo pesquisa sobre este assunto [da
    segurança cibernética] -- algo que desbalanceia o debate e insere a
    segurança cibernética como algo para os sistemas, em vez de para as
    pessoas. Mas a segurança cibernética é intrinsecamente sobre pessoas. Como
    uma área da política interessada na regulamentação do comportamento
    *online*, o modo como ela é definida implementada terá -- e já está tendo
    -- implicações profundas para direitos humanos essenciais como a
    privacidade e a liberdade de expressão.

    [...] Acima de tudo, precisamos lutar por uma abordagem aberta, inclusiva e
    multissetorial da elaboração de políticas. Em uma sociedade
    democrática, a implementação da segurança cibenrética demanda o
    consentimento informado da população -- o que significa garantir que vozes
    fora das agências de segurança estejam envolvidas no debate.

    [...] A segurança cibernética é uma questão que chega na própria essência
    do que é a internet. A internet nunca foi, no fim das contas, feita para
    ser segura -- por *design* ela é interoperável, multijurisdicional e
    horizontal, qualidades que raramente conduzem à segurança. Mas são estas
    qualidades que a tornam valiosa e fazem valer a pena lutar por ela. Se
    queremos que ela continue desta maneira, esse é um debate que não podemos
    nos dar o luxo de evitar. [PUDDEPHATT-KASPAR-CIBERSEG-2015]_



Brasil
======

No Brasil, as principais estruturas de segurança cibernética são o os órgãos de inteligência e defesa cibernética, ligados ao Ministério da Defesa e ao GSI/PR (CDCiber, Abin), a Polícia Federal, e os grupos de tratamento de incidentes de segurança brasileiros -- como o CERT.br, aberto a todo o público brasileiro e vinculado ao CGI.br, o CTIR-GOV, que serve a Administração Pública Federal, e outros específicos como o GRIS-Correios, CSIRT CAIXA, CSIRT UOL e CSIRT Unicamp.

.. figure:: imagens/segciber.png
    :align: center
    :width: 300px
    :figwidth: 300px

    Visão em camada da estratégia brasileira de segurança da informação e comunicação, de segurança e defesa cibernética. [BRASIL-CIBERSEG-2015]_

.. figure:: imagens/cert_mapa.png
    :align: center

    Mapa dos grupos de tratamento de incidentes de segurança brasileiros, feito em junho de 2013 e `apresentado por Cristine Hoepers <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos/documentos/ApresentaoCristineHoepers.pdf>`_, gerente geral do CERT.br/NIC.br, em setembro de 2015.


O estudo e a prática da segurança cibernética pelo Estado brasileiro remonta à
criação do Gabinete de Segurança Institucional (GSI/PR) pela Presidência da
República, através do Decreto no 3.505/2000. Órgãos associados ao gabinete
tratar de questões específicas da "defesa cibernética" foram criados nos anos
subsequentes. [BRASIL-CIBERSEG-2015]_

Um dos principais catalisadores da infraestrutura de defesa cibernética
brasileira foi a CPI da Espionagem, "destinada a investigar a denúncia de
existência de um sistema de espionagem, estruturado pelo governo dos Estados
Unidos, com o objetivo de monitorar e-mails, ligações telefônicas, dados
digitais, além de outras formas de captar informações privilegiadas ou
protegidas pela Constituição Federal".

Uma das recomendações da Comissão Parlamentar de Inquérito que foram levadas à cabo pelo governo federal foi a elaboração de uma Estratégia Nacional de Segurança Cibernética, em que "sejam delineadas as principais medidas de segurança cibernética para o Estado brasileiro, englobando ações coordenadas entre os setores público e privado".

No documento resultante, escrito pelo Departamento de Segurança da Informação e
Comunicações (DSIC) e sugestivamente nomeado  "Estratégia de Segurança da
Informação e Comunicações e de Segurança Cibernética da Administração Pública
Federal", conta-se a história da segurança da informação e cibernética na estratégia de defesa do país através de "marcos do governo brasileiro em SIC e SegCiber".

Após a criação do GSI/PR, foram estabelecidas ao longo dos anos diversas
parcerias com setores específicos do governo, como a Controladoria Geral da
União, o Banco do Brasil e a Caixa Federal, a Petrobrás, o INSS e a SERPRO,
"com a finalidade de organizarem atividades em conjunto que possibilitassem a
disseminação da cultura da Segurança da Informação". Também são registrados os
acordos de "Troca e Proteção Mútua de Informações Classificadas" que o Brasil
assinou com Portugal, Espanha, Rússia, Itália, Israel e Suécia.

Na seção "Contextualização", os(as) autores(as) contam com mais detalhes o
crescimento dos órgãos de apoio ao GSI/PR:


    No Brasil, os assuntos relacionados à Segurança da Informação e
    Comunicações, Segurança Cibernética e Segurança das Infraestruturas
    Críticas vêm sendo tratados no âmbito do **Conselho de Defesa Nacional (CDN)**
    e da **Câmara de Relações Exteriores e Defesa Nacional (CREDEN)**, do Conselho
    de Governo, por intermédio do **Gabinete de Segurança Institucional da
    Presidência da República (GSI/PR)**, que exerce as funções de Secretaria
    Executiva do citado Conselho e de Presidência daquela Câmara.

    [...]

    A dimensão e a assimetria da APF representa importante desafio para a área
    de SIC e de SegCiber. Na atualidade, são 39 ministérios, cerca de seis mil
    entidades governamentais, mais de um milhão de servidores federais, em
    torno de 320 grandes redes do Governo Federal, mais de 16,5 mil sítios de
    governo que superam 12 milhões de páginas WEB, e uma crescente participação
    e controle social.

    O GSI/PR, diante de tal desafio, instituiu em 2006, para trato das questões
    afetas à SIC e à SegCiber, **o Departamento de Segurança da Informação e
    Comunicações (DSIC), com abrangência de atuação na APF**, e três áreas
    finalísticas para o cumprimento de sua missão, a saber: **Gestão de SIC**,
    **Centro de Tratamento de Incidentes de Redes da Administração Pública
    Federal - CTIR Gov**, e **Credenciamento de Segurança**.

    [...]

    A **Agência Brasileira de Inteligência (ABIN)**, órgão vinculado ao GSI/PR,
    conta em sua estrutura com o **Centro de Pesquisas e Desenvolvimento para
    Segurança das Comunicações (CEPESC)**, criado em 1982 para sanar deficiência
    do Brasil em garantir o sigilo dos canais de comunicação dos órgãos
    estratégicos da Administração Pública Federal. Desde então, vem
    desenvolvendo **soluções de segurança da informação e comunicações** baseadas
    em algoritmos criptográficos de Estado, bem como executando **trabalhos de
    pesquisa e desenvolvimento** na área da segurança cibernética.

    Assim, na última década, os **temas de SIC e de SegCiber passaram a ser**
    reconhecidos por vários atores do Governo Federal como relevantes e **de
    competência e coordenação político estratégica de órgão da Presidência da
    República**, com abrangência para a APF, incluídas ações de segurança das
    infraestruturas críticas da informação. [BRASIL-CIBERSEG-2015]_

.. figure:: imagens/segciber_brasil.png
    :align: center
    :width: 400px

    Organograma do sistema institucional de segurança e defesa cibernética brasileiras. [BRASIL-CIBERSEG-2015]_

A história e a organização institucional da segurança e da defesa cibernética no Brasil é muito bem contada em Da Cibersegurança à Ciberguerra [ARTIGO19-CIBERSEG-2016]_, uma análise crítica da ONG ARTIGO 19 sobre o uso potencial de equipamentos e softwares de vigilantismo ou ofensivos como parte da estratégia nacional de defesa cibernética.


.. figure:: imagens/segciber_artigo19.png
    :align: center
    :width: 400px
    :target: http://artigo19.org/wp-content/blogs.dir/24/files/2016/03/Da-Ciberseguranc%CC%A7a-a%CC%80-Ciberguerra-WEB.pdf

    Relatório Da Cibersegurança à Ciberguerra, da ARTIGO 19.

Cenário internacional
=====================


Algumas organizações internacionais se destacam tanto por sua abordagem no âmbito da segurança cibernética e da informação quanto pela participação do Brasil em seus processos de deliberação e decisão multissetorial (onde são ouvidos tanto representantes de governos quanto de empresas, da sociedade civil, academia e outros setores da sociedade) .

.. _oea:

Organização dos Estados Americanos (OEA / OAS)
----------------------------------------------


Segundo conta Carolina Rossini, vice-presidente e coordenadora de política internacional da organização de acesso à informação Public Knowledge, o fortalecimento da segurança cibernética de seus países-membro é uma das prioridades da OEA:

    Em resposta ao aumento das ameaças cibernéticas identificadas, a
    Organização dos Estados Americanos (OEA), através do Comitê Interamericano
    Contra o Terrorismo (CICTE), vem desenvolvendo um programa regional de
    segurança cibernética que busca fortalecer a segurança e melhorar a
    proteção da infraestrutura de informação crítica em todo o continente.

    Assim, desde o começo da década da passada, a OEA investe na segurança cibernética
    por meio de uma série de atividades em parceria com especialistas, empresas
    e governos da região. Em 2004, a OEA publicou a "Declaração sobre o
    Fortalecimento da Segurança Cibernética nas Américas", pedra fundamental
    para o desenvolvimento e a publicação do relatório "Estratégia
    Interamericana Integral para Combater as Ameaças à Segurança Cibernética",
    em 2012.

    A declaração de 2004 significou o reconhecimento por parte dos Estados
    Membros da OEA de que combater os crimes cibernéticos e fortalecer a
    resiliência cibernética eram questões imperativas para o desenvolvimento
    econômico e social, a governança democrática, a segurança nacional e dos
    cidadãos. [ROSSINI-OEA-2015]_

Além de publicar `relatórios <https://www.sites.oas.org/cyber/Certs_Web/OAS-Trend%20Micro%20Report%20on%20Cybersecurity%20and%20CIP%20in%20the%20Americas.pdf>`_ sobre a situação da segurança cibernética no continente americano e `guias de boas práticas <https://www.symantec.com/content/es/mx/enterprise/other_resources/b-cyber-security-trends-report-lamc.pdf>`_, a OEA também articula atividades e eventos em parceira com organizações de segurança para promover a cultura de segurança e melhorar estruturas de resposta a incidentes.

O governo brasileiro, através do Gabinete de Segurança Institucional, dialoga com a OEA na área digital, participando inclusive de eventos estratégicos, como é relatado no Livro Verde: Segurança Cibernética no Brasil:

    Uma iniciativa que vale frisar é a adoção, pela OEA, desde 2004, de uma
    "Estratégia Interamericana Integral para Combater as Ameaças à Segurança
    Cibernética", visando a criação de uma cultura de segurança cibernética
    para lutar contra as ameaças aos cidadãos, à economia e aos serviços
    essenciais, que não possam ser enfrentadas por um único governo ou
    combatidas por meio de uma disciplina ou prática solitária. No ano de 2009,
    o "Workshop Hemisférico Conjunto da OEA sobre o Desenvolvimento de uma
    Estrutura Nacional para Segurança Cibernética" foi realizado de 16 a
    20/Nov/2009, contando na organização, além do Governo brasileiro,
    anfitrião, por intermédio do GSIPR, da OEA representada pelos seguintes
    fóruns: Comitê Interamericano contra o Terrorismo Cibernético (CICTE),
    Comissão Interamericana de Telecomunicações (CITEL), e, Reunião de
    Ministros da Justiça ou Procuradores Gerais das Américas (REMJA), o que
    fortaleceu o papel do Brasil como um dos protagonistas no tema; [BRASIL-VERDE-2010]_



Um dos últimos relatórios sobre segurança cibernética publicados pela OEA em parceria com o Banco Interamericano de Desenvolvimento  (BID / IDB) é o "Cybersecurity: Are We Ready in Latin America and the Caribbean?". Em um prefácio, o secretário geral da OEA, Luis Almagro, conta sobre o programa de segurança cibernética da OEA se origina de seu comitê antiterrorismo (tradução nossa):

    O Programa de Segurança Cibernética [Cybersecurity Program] da Comitê
    Interamericano contra o Terrorismo (CICTE).teve um papel chave nesta
    frente. O programa ajudou os Estados Membros a desenvolver Estratégias
    Nacionais de Segurança Cibernética, providenciou treinamento para a Equipes
    de Resposta a Incidentes de Segurança de Computadores [Computer Security
    Incident Response Teams] (CSIRT), facilitou exercícios de gestão de
    crise com operadores da indústria nacional crítica e setores de resposta de
    emergência, se engajou com a sociedade civil e o setor privado, e ajudou a
    trazer visibilidade à ameaças relacionadas à segurança da informação e
    oportunidades dentro da nossa região. Dessas e outras maneiras, o CICTE tem
    diretamente contribuído para um domínio cibernético mais seguro e vigilante
    no Caribe e na América Latina. [OEAeBID-CIBERSEGLATAM-2016]_

.. figure:: imagens/ciberseg_oea-bid_brasil.png
    :align: center
    :target: /_images/ciberseg_oea-bid_brasil.png

    Perfil da segurança cibernética do Brasil em ampla análise da América
    latina e do Caribe feita pela OEA e o BID, a partir de contribuições do
    governo (ABIN, DSIC, CTIR-GOV, GSI/PR) e da sociedade civil (CTS/FGV,
    CGI.br, ITSRio) [OEAeBID-CIBERSEGLATAM-2016]_ (páginas 60 e 61).

A Public Knowledge, no módulo Ciberseguridad y Derechos Humanos de seu curso Internet Libre y Abierto da P2PU, analisa a atuação da OEA na seção "segurança cibernética e relações internacionais" (tradução nossa):

    Em 2004, os Estados Membros da OEA adotaram de forna unânime a
    `Estratégia Interamericana Integral para Combater as Ameaças à Segurança Cibernética <http://www.oas.org/es/ssm/cyber/documents/Estrategia-seguridad-cibernetica-resolucion.pdf>`_;
    como a maioria das estratégias sobre segurança cibernética, ela tem o foco
    principal no crime e on terrorismo cibernético, deixando de lado outros
    aspectos da segurança cibernética. O espírito da resolução revela uma tendência
    progressiva na América Latina para a aplicação não só da doutrina da justiça
    penal e da segurança pública, mas também da doutrina militar, nas estratégias
    de segurança cibernética, e assim destaca a
    `Análise de referência de processos e eventos relativos às TIC: Implicências para a segurança internacional e regional <http://ict4peace.org/baseline-review-of-ict-related-processes-and-events-implications-for-international-and-regional-security/#sthash.8X8nxU44.dpuf>`_.

A OEA vem dialogando com a sociedade civil em seus relatórios, eventos e treinamentos. No relatório Cybersecurity citado acima, há uma *expert contribution* escrita por pesquisadores(as) do Centro de Tecnologia e Sociedade da Fundação Getúlio Vargas (CTS/FGV): Cybersecurity, Privacy and Trust: Trends in Latin America and the Caribbean ("Segurança Cibernética, Privacidade e Confiança: Tendências na América Latina e no Caribe").

Na contribuição, Marília Maciel, Nathalia Foditsch, Luca Belli e Nicolas Castellon aponta algumas características do campo na região: conscientização da importância de estratégias de segurança cibernética; o exército e agências de segurança nacional não foram amplamente estabelecidas como coordenadores das políticas; há alguma colaboração multissetorial notável pela presença e colaboração entre CSIRT's (equipes de resposta a incidentes); muitos países vêm alterando leis de forma precipitada para o combate ao crime cibernético; há um aumento substancial na preocupação com a privacidade e a proteção dos dados pessoais -- que, segundo o documento, é incompatível com a retenção de dados em massa [OEAeBID-CIBERSEGLATAM-2016]_.

Para o caminho à frente no campo da segurança cibernética, o grupo recomenda (traduções nossa):

- **Definir e fazer valer marcos regulatórios coerentes de privacidade e proteção de dados**: "é essencial balancear a provisão de segurança com a necessidade de proteger aprorpiadamente os direitos dos indivíduos. [...] Também é necessário balancear os custos e benefícios da existência de provisões de retenção de dados."


- **Criar plataformas nacionais multissetorial sustentáveis**: "grupos da sociedade civil, as comunidades acadêmicas e técnicas, e representantes da indústria são capazes de prover *expertise* valiosos através de suas perspectivas, e ajudar a criar marcos regulatórios coerentes de uma maneira sustentável."

- **Fortalecer a cooperação internacional**: "é importante criar canais para cooperação em vários níveis entre governos nacionais e organizações regionais ou globais trabalhando na área. [...] a natureza da Internet de não ter fronteiras aumenta a importância da cooperação internacional e da harmonização de dispositivos legais."


Organização para Cooperação e Desenvolvimento Econônico (OECD / OCDE)
---------------------------------------------------------------------

A OECD, fundada em 1961 para estimular o progresso econômico e o comércio global. Entre diversos tópicos como educação, ciência e tecnologia, agricultura e relações públicas, a organização também aborda a segurança digital.



Através de um processo multissetorial iniciado em 2012 pelo Working Party on Security and Privacy in the Digital Economy ("grupo de trabalho em segurança e privacidade na economia digital"), a OECD publicou o documento Recommendation on Digital Security Risk Management ("recomendações sobre gestão de riscos de segurança"), onde delineia os caminhos que os Estados devem trilhar para proteger suas infraestruturas e processos críticos de ataques digitais em harmonia com direitos fundamentais, em torno de 8 princípios (traduções nossas):

* **Percepção, habilidades e empoderamento** (*awareness, skills and empowerment*)
* **Responsabilidade e prestação de contas** (*responsibility*)
* **Direitos humanos e valores fundamentais** (*human rights and fundamental values*)
* **Cooperação** (*co-operation*)
* **Reconhecimento de riscos e ciclos de tratamento** (*risk assessment and treatment cycle*)
* **Medidas de segurança** (*security measures*)
* **Inovação** (*innovation*)
* **Prestatividade e continuidade** (*preparedness and continuity*) [OECD-RISK-2015]_

A OECD prefere tratar o tema através do conceito de *riscos de segurança digital*, em vez da segurança cibernética, por sua maior clareza e por trazer a segurança de um tema meramente técnico para o econômico e social (tradução nossa):

    Em vez de ser tratado como um problema técnico que pede soluções técnicas,
    o risco digital deve ser abordado como um risco econômico; ele deve então
    ser uma parte integral dos processos gerais de gestão de risco e tomada de
    decisão de uma organização. A noção de que riscos de segurança digital
    merecem uma resposta fundamentalmente diferente em sua natureza das outras
    categorias de riscos precisa ser combatida. Para este efeito, o termo
    "segurança cibernética" e de forma mais geral o prefixo "ciber" que ajudaram
    a transmitir este senso errôneo de especificidade não aparecem nas
    Recomendações de 2015.

Embora as recomendações não tenham efeito legal vinculante ("*non-legally binding*"), o peso da OECD como organização é claro. Os atos da organizção "na prática possuem grande força moral, pois representam a vontade política dos países membros", segundo o documento anexo às Recomendações:

    É importante destacar que as Recomendações, e de modo mais geral o trabalho
    da OECD na área [de segurança digital], é parte de um diálogo internacional
    envolvendo várias organizações, com fluxos complementares de trabalho
    refletindo seus mandatos. Por exemplo, o Conselho da Europa endereçou
    questões relacionadas ao crime cibernético (por exemplo, na Convenção de
    Budapeste sobre o Crime Cibernético); a Interpol facilita a cooperação
    operacional entre agentes da lei; as Nações Unidas e a Organização para
    Segurança e Cooperação na Europa (OSCE) discute o comportamento dos Estados
    no meio digital e medidas de construção de confiança para preservar a
    estabilidade nacional; padrões técnicos estão sendo desenvolvidos em uma
    variedade de ambientes como a International Organization for
    Standardization (ISO), a Internet Engineering Task force (IETF), a World
    Wide Web Consortium (W3C), a Organization for the Advancement of Structured
    Information Standards (OASIS), etc. Organizações regionais como a
    Asia-Pacific Economic Cooperation (APEC) também desepenham um papel-chave
    para estimular a implementação das melhores práticas e orientações. [OECD-RISK-2015]_

Embora o Brasil não seja um país membro da OECD, em 2007 seu Conselho Ministerial estabeleceu um acordo de "engajamento aprimorado", como um esforço originado em 2003 pelo Embaixador do Japão para a OECD Seiichiro Noboru para alargar a cooperação com países não-membros. Além do Brasil, também firmaram o acordo a China, Índia, Indonésia e África do Sul, selecionadas através de quatro critérios: "afinidade de pensamento" (*like-mindedness*), "ator relevante" (*significant player*), "benefício mútuo" (*mutual benefit*) e "considerações globais" (*global considerations*). [OECD-ENGAGEMENT-2015]_.

O Brasil `assinou <https://www.blevinsfranks.com/News/BlevinsFranks/BlevinsFranksNews?ArticleID=722>`_ em 2014 um "tratado multilateral de autoridade competente" estabelecido em reuniões da OECD para combater a sonegação de impostos. O tratado prevê o compartilhamento de dados financeiros sobre impostos com os outros países participantes; o Brasil começará a incluir os seus a partir de 2018.

Mas talvez seja no campo da segurança cibernética que se dá a maior participação do Brasil na OECD.

Já em 2010, na publicação do Livro Verde: Segurança Cibernética no Brasil pelo GSI/PR, as recomendações da OECD (na prática, uma versão anterior da supracitada [OECD-RISK-2015]_) eram citadas como ponto de partida para compreender as "competências essenciais da segurança cibernética".

No mesmo documento, a participação do Brasil em um evento da OECD é citado como exemplo do Brasil sendo "um dos protagonistas em iniciativas e fóruns internacionais" e de sua "competência articuladora, de gestão e técnica" no tema:

    A participação de representante do Brasil, do GSIPR, na categoria de
    observador ad hoc no "Working Party on Information Security and Privacy -
    WPISP", e do "Comittee for Information, Computer and Communications -
    ICCP", promovidos pela "Organização para Cooperação e Desenvolvimento
    Econômico - OCDE", realizados em Paris/França, em 2009 e 2010, também
    merece destaque. Por ocasião da reunião de 2010, o Brasil apresentou
    proposta de realização de "Estudo comparativo das estratégias nacionais de
    segurança cibernética"; a qual foi plenamente aceita e, para tanto, foi
    criado Grupo com presença de países voluntários para tal finalidade. O
    Grupo é presidido pelo representante de Portugal na OCDE, e conta com a
    participação dos seguintes países: Portugal, EUA, Coréia, Austrália, Japão,
    Espanha, e Brasil. [BRASIL-VERDE-2010]_

A sociedade civil brasileira também tem se aproximado da OECD no que tange
políticas de segurança cibernética e gestão de riscos de segurança digital. Na organização, o diálogo com a sociedade civil se dá através do CSISAC, Civil Society Information Society Advisory Council, criado a partir de uma declaração feita na Conferência Ministerial sobre o Futuro da Economia da Internet, ocorrida em Seoul, Coréia do Sul, em junho de 2008.

Além do pedido de criação de um conselho da sociedade civil, a Declaração de Seoul também chama a atenção dos(as) Ministros(as) para diversos temas, trazendo recomendações específicas para cada um (tradução nossa):

- **Liberdade de expressão**
- **Proteção da privacidade e transparência**
- **Proteção do(a) consumidor(a)**
- **Emprego, trabalho digno e habilidades**
- **Promoção do acesso ao conhecimento**
- **Governança da Internet**
- **Promoção de padrões abertos e neutralidade da rede**
- **Políticas de direitos autorais balanceadas**
- **Apoio a mídias pluralísticas**
- **Sociedade digital inclusiva**
- **Diversidade cultural** [OECD-SEOUL-2008]_

Em junho deste ano, a OECD realizará seu encontro Ministerial anual, reunindo os ministros da economia de todos os países membro. Com o foco principal na Economia Digital, o Ministerial será dividido em quatro tópicos, sendo um deles a Confiança na Economia Digital, com um painel intitulado Gerindo o Risco Digital (a OECD prefere esse termo à "segurança cibernética", como já explicado). O Brasil será representado, através do CSISAC, por organizações sem fins lucrativos como a ARTIGO 19, Coding Rights, InternetLab, Instituto Beta para Internet e a Democracia e CTS/FGV, que constam como Grupos de Referência do `CSISAC Forum <http://csisac.org/events/cancun16/>`_, um encontro realizado junto com o Ministerial que reunirá a sociedade civil organizada para deliberar a atuação do CSISAC no evento principal.


International Telecommunication Union (ITU / UIT)
-------------------------------------------------

A União Internacional das Telecomunicações, sediada em Genebra, Suíça, possui a missão de orientar o crescimento e desenvolvimento sustentável das telecomunicações e redes de informação, e possui 192 Estados-membros, incluindo o Brasil.

Como vimos, a organização conduziu um estudo sobre segurança cibernética entre 2005 e 2008, materializado em sua Recommendation ITU-T X.1205, "Visão geral da segurança cibernética", onde definem o conceito e analisam estratégias de proteção e técnicas de ataque. [ITU-CIBERSEG-2008]_


A UIT também aborda o campo através de sua `Global Cybersecurity Agenda <http://www.itu.int/en/action/cybersecurity/Pages/gca.aspx>`_ (tradução nossa):

    Lançada em 2007 pelo então Secretário-Geral da UIT, Dr. Hamadoun I. Touré
    (2007 - 2014), a ITU Global Cybersecurity Agenda (GCA) é um sistema para
    cooperação internacional destinado a aumentar a confiança e a segurança na
    sociedade da informação. A GCA foi projetada para a cooperação e a
    eficiência, encorajando a colaboração com e entre todos os parceiros
    relevantes e com base em iniciativas existentes para evitar a duplicação de
    esforços. [ITU-GCA]_

.. figure:: imagens/segciber_gca.png
    :align: center

    Ilustração dos cinco pilares estratégicos, ou áreas de trabalho (*work areas*) da Global Cybersecurity Agenda: **medidas legais, medidas técnicas e procedurais, estruturas organizacionais, capacitação e cooperação internacional**.

