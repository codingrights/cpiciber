=================================
Resposta ao Relatório da CPICIBER
=================================


Conceitos
=========

O relatório introduz (na pág. 61) os conceitos que julga importantes "quando se cuida de analisar a Internet e todas as circunstâncias a ela relacionadas". Embora seja dito que a lista foi compilada "sem a pretensão, obviamente, de exaurir o tema", verificamos que os termos da lista de definições (que ocupa 8 páginas do relatório) foram **escolhidos de forma arbitrária**, muitas vezes sem que o termo sequer tenha sido mencionado nas audiências da comissão. Dos 21 conceitos apresentados, destacamos estas 11 por terem poucas ou zero menções na CPI, conforme obtido por análise das notas taquigráficas:

- **engenharia social**: 5 menções de passagem
- **hoax**: nenhuma menção
- **worm**: 1 menção de passagem
- **deface**: 3 menções
- **spyware**: 3 menções de passagem
- **cavalos de troia / trojan horses**: 3 menções de passagem
- **botnet**: 3 menções
- **keylogger**: nenhuma menção
- **sniffer**: nenhuma menção
- **mail bomb**: nenhuma menção
- **WikiLeaks (???)**: 3 menções, sendo duas delas do próprio relator, o deputado Espiridião Amin (que `afirmou <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-
temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-
ciberneticos/documentos/notas-taquigraficas/nt221015-crc-sem-revisao>`_ que Julian Assange e Chelsea Manning "abriram caminhos em matéria de liberdade").


A presença destes termos contrasta com a **ausência** de outros termos muito mais básicos para o tema investigado pela Comissão Parlamentar. Contamos, através de análise das notas taquigráficas, muito mais menções para alguns termos-chave que não foram definidos nos conceitos importantes nem melhor explorados nas seções subsequentes do relatório:

- **"segurança cibernética" ou "cibersegurança"**: 63 menções
- **"defesa cibernética"**: 69 menções
- **"segurança da informação"**: 138 menções
- **"anonimato" ou "anônimo" ou "anônima"**: 156 menções
- **"privacidade"**: 225 menções
- **"criptografia" e prefixo 'criptograf-'**: 263 menções
- **"liberdade de expressão"**: 102
- **"cibernética" e prefixo 'ciber-' de modo geral**: 1137 menções

Estes termos, tão importantes quanto confundidos, muitas vezes possuem múltiplas definições; seria muito mais interessante defini-los, ao menos internamente ao relatório e à CPI, para que tivéssemos um entendimento comum ao debater sobre eles.

No caso do termo **criptografia**, tão presente nas audiências da CPI e nas mazelas do sistema judicial e da polícia no Brasil, houve inclusive demanda por parte do especialista em Segurança da Informação Arthur César Oreana, que `falou <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-
temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-
ciberneticos/documentos/notas-taquigraficas/nt261115-crc-sem-revisao>`_ para a CPI em 26/11/2015:  "[...] achei
importante, antes de começar esta reunião, dizer a palavra 'criptografia'. Tudo gira
em torno dela. Eu acho interessante que no decorrer aqui das reuniões vocês possam analisar melhor essa palavra, seu significado."

Mesmo dentro dos termos que os(as) relatores(as) resolveram definir, há confusões e inconsistências. O termo **códigos maliciosos** é introduzido e algumas possibilidades de se infectar um computador são listadas ("exploração de vulnerabilidades existentes em programas", "execução de arquivos previamente infectados, obtidos em anexos de mensagens eletrônica, via mídias removíveis (...)"), e após isto são definidos mais três termos que deveriam ser subcategorias do primeiro: **worm**, **spyware**, **cavalo de troia** e **backdoor**.

Não obstante a falta de clareza na taxonomia, as próprias definições são confusas: Embora a definição do relatório para *backdoor* seja equivalente à do *cavalo de troia*, **18 dentre as 19 menções a *backdoor* nas audiências são sobre seu outro significado**: um acesso escondido, deixado pela fabricante de um *hardware* ou desenvolvedora de um *software* ou algoritmo, deliberadamente ou sob coerção, como no caso das empresas citadas nos *slides* da NSA como alvos de seu programa BULLRUN, que inseria agentes nos órgãos de padronização de protocolos de criptografia para enfraquecê-los de modo que a NSA pudesse quebrar as cifras mais rapidamente.

Deixando de fora discussões importantes que foram trazidas à CPI por convidados como Cristiana Gonzalez, representante do IDEC, Carlos Eduardo Sobral, da Secretaria Extraordinária de Segurança em Grandes Eventos do Ministério da Justiça, e Marcos Mazoni, Diretor-Presidente da SERPRO.

.. figure:: imagens/backdoors_serpro.png
    
    Slides apresentados por Marcos Mazoni mostram a preocupação da SERPRO com *backdoors* -- mas não aqueles definidos pelo relatório.

Cristine Hoepers, gerente-geral do CERT.br , também manifestou sua preocupação com *backdoors*:

    Hoje a gente sabe que alguns governos — principalmente os da polícia
    americana e polícias da Inglaterra — têm esse conceito de que as polícias podem
    quebrar ou ter acesso, podem ter uma espécie de *backdoor*, uma criptografia. Ou a
    criptografia é forte para todos ou ela não o é para ninguém. No momento em que
    você coloca um ponto de entrada, uma quebra, um *backdoor* para que alguém tenha
    acesso, isto a torna fraca para todos.

Além disso, não há menção alguma ao uso de *malwares*, ou códigos maliciosos,
por parte da polícia e do judiciário. Sabemos, através de vazamentos de e-mails
da empresa italiana Hacking Team, que vários setores do governo negociaram com
a empresa ferramentas de invasão de computadores e celulares, tendo a Polícia
Federal adquirido softwares espiões (*cavalos de troia*, para usar um termo
definido pelo relatório).

Segundo o relatório
`Vigilância das Comunicações pelo Estado Brasileiro <http://www.internetlab.org.br/wp-content/uploads/2016/01/ILAB_Vigilancia_Entrega_v2-1.pdf>`_,
feito pelo InterletLab e a Electronic Frontier Foundation, "a pesquisa nos
arquivos [de e-mails vazados], ainda que superficial, revela trocas de emails
entre agentes [da Polícia Federal] e funcionários da Hacking Team, relatos
sobre treinamentos em Brasília, e diversos documentos, como um certificado de
entrega de produto, que confirmam a negociação e aquisição da 'solução' RCS
(Remote Control System) da Hacking Team para um projeto piloto de 3 meses de
duração".

Conforme :ref:`dissemos <invasao>`_ nesta nota técnica há uma série de riscos
no uso de softwares de invasão e exploração de vulnerabilidades por parte do
poder público: a prática, por definição, não permite nenhum tipo de limitação
sobre as ações da autoridade a conduzir a invasão, ou registro confiável de
suas atividades uma vez dentro do sistema; além disso, o acesso a um
dispositivo nos dias de hoje representa uma invasão de privacidade muitas vezes
maior que a busca e apreensão em uma casa.




Participação na CPI
===================

.. image:: imagens/cpiciber_stakeholders.png
    :width: 400px
    :align: center


Deep Web
========

1.1.3.21 – Deep Web
Assim como o ARPANET, a internet profunda é derivada70
de aplicações desenvolvidas inicialmente para o âmbito militar. Suscintamente,
a deep web é a zona da internet em que os sites, fóruns e comunidades não
podem ser detectados pelos tradicionais mecanismos de busca (como o
Google e o Bing, por exemplo). Para se acessar a deep web são necessários
sistemas operacionais e programas específicos que acessam servidores
dedicados que funcionam como porta de entrada para esses domínios não
indexados pelos sistemas abertos.

Deep Web e terrorismo
=====================


Apesar desse panorama esparso de leis em voga no País,
discute-se acerca da necessidade de uma lei que trate apenas dos crimes
cibernéticos, ao tempo em que novos órgãos e entidades surgem para
disciplinar o sistema, diante de novas espécies delinquenciaisO uso da
chamada deep web ou dark net e novas ameaças, como o terrorismo virtual,
reforçam esses argumentos

Terrorismo
==========


    Seminário Segurança Cibernética para as Olimpíadas Rio 22.5.016

    Outra questão muito relevante, e que foi levantada pelo
    Dep. Delegado Eder Mauro em algumas audiências da CPI, diz respeito à
    preparação do país para receber as Olimpíadas de 2016, tendo em vista o
    receio de que tal evento esportivo seja utilizado como palco para ataques
    terroristas, que podem ser praticados ou organizados, inclusive, por meios
    cibernéticos.

    Em que pese essa temática não ter recebido o tratamento
    que merecia devido à importância e à premência do evento, esta CPI reservou
    um espaço especial em sua agenda e realizou Seminário específico tendo
    convidado as principais autoridades a cargo da segurança cibernética do
    grande evento. No Seminário, resultado de requerimento dos Deputados
    Delegado Eder Mauro e Silas Freire, foi apresentado o arcabouço institucional
    desenvolvido para o acompanhamento do evento e as principais ações em
    andamento, além das ferramentas que serão utilizadas durante os jogos.

    Entretanto, os depoimentos indicaram que a questão é extremamente
    intrincada, envolve recursos tecnológicos complexos e humanos intensivos.
    Ademais, o potencial de dano é altíssimo, ainda mais em se considerando os
    atuais acontecimentos de terrorismo internacional, como as ações do Estado
    Islâmico. Dessa forma, recomendamos à Comissão de Segurança Pública e
    Combate ao Crime Organizado que crie uma Subcomissão Especial para
    acompanhar essa questão.


O caso Snowden
==============


    O chamado Caso Snowden foi um escândalo de espionagem revelado pelo
    ex-funcionário da agência americana de inteligência, Central Intelligence
    Agency (CIA), Edward Snowden. O fato de que as agências de inteligência
    interceptaram mensagens e dados inclusive da presidente Dilma Rousseff e
    da Petrobras teve como consequência a imposição de regime de urgência na
    tramitação do Projeto de Lei do Marco Civil da Internet na Câmara dos
    Deputados, trancando inclusive, a partir de outubro, a votação de
    qualquer outro projeto na casa legislativa.



foi não, inocente, continua sendo!


O episódio demonstra uma dificuldade observada no caso das investigações
levadas a efeito no Brasil, que é a sistemática recusa dos provedores de
conteúdo em atenderem às requisições das autoridades policiais, sob a
alegação de que os dados estariam sediados no país de origem. Tal fato é
mais comum no tocante à rede social Facebook, que possui milhões de
clientes no Brasil e mundo afora. Os depoimentos à CPI evidenciaram
diversos conflitos entre autoridades judiciais e as principais empresas
ponto com.

WAT??


Boas práticas
=============

    É extremamente importante que a conduta individual não seja geradora
    de oportunidades aos delinquentes virtuais. Assim, a chamada
    prevenção situacional aplicável ao mundo real é perfeitamente
    amoldável ao ambiente cibernético.

    Ao se abordar boas práticas é possível entrevê-las como iniciativas
    institucionais, de entes públicos ou privados, assim como iniciativas
    individuais. Nesse caso, a mencionada prevenção situacional ganha
    vulto, na medida em que cada indivíduo seja responsável por sua
    segurança on line.


Massa!...

    - Tornar prática inquestionável a requisição de
      dados cadastrais pelas autoridades policiais; 

???? faz menção à lei de organizações criminosas somente

organizações criminosas - terrorismo


    - Instituir um sistema de identificação civil único
      (identificação unívoca);

...


    - Bloqueio e a retirada de páginas;

isso não resolver amiga

    - Aderir à Convenção de Budapeste;

NAAO


    - Conferência Octopus contra o Cibercrime;



    - Proteger as vítimas de pornografia infantil,
      interpelando os compartilhadores;

    - Instituir uma agência reguladora de software;

    - Novas leis para combater mais efetivamente,
      inclusive o que é terrorismo digital;

    - Atualizar a questão sobre o encarceramento
      digital e implementar o procedimento de polícia para revista digital

    - Decálogo do conteúdo gestor da Internet;

    - Testemunhas-máquinas (dever de colaboração
ágil);

    - Identidade digital obrigatória;

    - Restabelecer a autoridade policial [Nos Estados Unidos a preservação e
      apresentação de evidências em crimes flagrantes, no caso de crimes
      considerados mais graves — por isso temos que aumentar a gravidade de
      alguns crimes no Código Penal — ocorrem por ordem de autoridade policial,
      não por ordem judicial.]

    - Projeto Mapear (Polícia Rodoviária Federal);

    - Direito ao esquecimento;

    - Cadastro Nacional de Acesso à Internet.


---


    Um dos golpes mais comuns atualmente é o chamado
    falso sequestro, que pode gerar, além de prejuízos financeiros, elevada carga
    de estresse psicológico.


WAT??? prevenção contra phishing, uso de criptografia, crime de ódio, pornografia de vingança, cadê?

    Outra séria incidência do cibercrime que causa
    verdadeiras tragédias é o tráfico de pessoas, tráfico humano ou tráfico de seres
    humanos (TSH).



    1.4 – PROCEDIMENTOS DE CRIAÇÃO DE CONTAS E DE PERFIS NA INTERNET


    A identidade de usuários na internet nos dias de hoje é
    extremamente importante devido à miríade de serviços oferecidos pela grande
    rede e pelo alcance das informações. Com uma conta de e-mail válida
    compram-se produtos, ativam-se cadastros em redes sociais e habilitam-se
    aplicativos. Neste contexto, uma identidade virtual é até mais importante do
    que uma carteira de identidade. Quando analisados os crimes cibernéticos
    cometidos, na gênese do golpe muitas vezes encontram-se perfis falsos ou
    inválidos, o que dificulta, chegando a impossibilitar em inúmeros casos, a
    investigação criminal e a punição dos culpados.

    [...]

    Foi possível evidenciar que pode ser criada uma conta
    qualquer (pertencente a pessoas verdadeiras ou fictícias) informando apenas
    um número de celular válido que serve para demonstrar que se trata de uma
    pessoa e não uma máquina.

    Todos os três provedores adotam o procedimento de
    enviar um código via SMS para o número indicado na tela de cadastramento da
    conta, sendo que o usuário deve informar esse código para finalizar o
    procedimento. Além desse número de telefone, é necessário informar nome
    completo, sexo e data de nascimento, mas não existe procedimento para
    confirmar a veracidade dessas informações.

    Deste exercício simplificado e não exaustivo com relação
    aos aplicativos analisados, depreende-se que a integridade e a veracidade dos
    dados informados dependem fundamentalmente da qualidade do cadastro da
    telefonia celular, isto é a correta identificação do titular da linha. Preocupa o
    fato do cadastro do celular pré-pago ser bastante flexível e simplificado. Apesar
    da Lei n o 10.703/2003, que exige o cadastro dos usuários do pré-pago, é
    notório que os dados ali informados não são checados ou aferidos com o rigor
    necessário. O assunto do cadastro do pré-pago foi motivo de Audiência Pública
    específica no âmbito desta CPI e as conclusões daquele debate fazem parte
    das conclusões do Sub-Relator de Segurança Cibernética, que concluiu pelo
    oferecimento de uma Proposta de Fiscalização e Controle acerca do cadastro
    do pré-pago


Identidade digital obrigatória
==============================

Patrícia Peck Pinheiro

    Então, a sensação de crime, a sensação de insegurança é do cidadão, porque,
    juridicamente, quando vamos para o devido processo legal, na grande maioria dos
    casos não conseguimos a punição, por conta disso. Talvez, se houvesse mais
    enforcement para uma identidade digital obrigatória, para a apresentação
    dessas provas de forma imediata por ambientes de aplicação web...  Isso gera
    custo, não é? Então poderíamos até dizer que temos mais bandidos, mais
    criminosos digitais presos. Hoje ainda acaba preso aquele que sai correndo pela
    rua.  Esse nós estamos vendo, nesse caso já sabemos como fazer. Quanto ao
    resto, acho que o propósito da Comissão é justamente o de aprendermos juntos e
    implementarmos um plano de ação nesse sentido. E, sim, pelo menos um terço
    ainda depende de uma melhoria na legislação para pelo menos restabelecermos a
    autoridade policial.

Encarceramento e revista digital
================================

Patrícia Peck Pinheiro


    .. image:: imagens/revistadigital1.png
        :width: 400px
        :align: center

    O que é a revista digital? É que grande parte das provas hoje vão estar
    em um dispositivo de celular; é a possibilidade, como o Reino Unido fez
    nas Olimpíadas de Londres, que está aqui.

    Esta é uma matéria muito interessante para quem tiver interesse em olhar.

    .. image:: imagens/revistadigital2.png
        :width: 400px
        :align: center

    Estão aqui todas as referências de que você pega o celular, passa,
    conecta aqui, passa um software com palavras-chave e com imagens. Você
    não tira nada do celular, só vê se ali dentro tem uma evidência de algo
    que pode ser suspeito para um crime. Se sim, leva a pessoa para
    procedimento de averiguação; se não, a pessoa segue. Isso aqui só se pode
    fazer no Brasil se conseguirmos avançar em termos de lei, de legislação
    mesmo.

    Estes são os países que já possuem revista digital. Estão combatendo o
    crime eletrônico, estão ficando mais fortes.

    .. image:: imagens/revistadigital3.png
        :width: 400px
        :align: center

Pedofilia / pornografia infantil
================================


    2.2.2 – Inclusão no rol de crimes hediondos o crime
    de pedofilia na internet

    Esta CPI tem a consciência de que a internet e as redes
    sociais amplificam os danos causados por mensagens, imagens e vídeos
    veiculados com o intuito de denegrir, manchar a honra ou violar a privacidade e
    a intimidade das pessoas. Essa amplificação se dá pela facilidade da
    replicação infinita dos conteúdos digitais sem custo, pela impossibilidade de
    erradicação definitiva dos materiais e pela consequente incapacidade de
    qualquer possibilidade de direito ao esquecimento.

    Esses acontecimentos são ainda piores em se tratando
    de menores. Foram inúmeros os relatos de autoridades policiais e de entidades
    a esta CPI acerca das nefastas consequências para as crianças e suas famílias
    quando imagens de pedofilia são veiculadas pela internet. Foram relatados
    casos de suicídios, abandonos de lar, de empregos e escolas em decorrência
    da circulação de conteúdos criminosos. Esta CPI entende que a razão para a
    proliferação desse mal digital é a impunidade. Por isso, apoiamos a
    aprovação do Projeto de Lei 1.776/15, em tramitação, que tornam crimes
    hediondos os delitos ligados à pedofilia. (Parte III, 3)


****

    2.2.3 – Previsão de bloqueio, por meio de decisão
    judicial, dos sites que disponibilizam conteúdos ilícitos.

    A internet, como se sabe, desde o seu surgimento trouxe
    enormes benefícios à sociedade, em termos de conhecimento, relações
    sociais, dentre outros. Mas, infelizmente, da mesma forma que a internet pode
    ser utilizada – e de fato o é – para a realização de coisas boas, ela também
    vem sendo palco para a realização de diversos crimes, conforme amplamente
    demonstrado nessa CPI.

    Inclusive, não é novidade a existência se sites voltados
    quase que exclusivamente à disponibilização e distribuição de conteúdos
    ilícitos.

    Dessa forma, mostra-se importante inserir no Marco Civil
    da Internet uma exceção à regra geral de neutralidade de rede que ratifique ao
    poder judiciário brasileiro a possibilidade de determinar aos provedores de
    conexão medidas técnicas de bloqueio de tráfego.

    Assim, ainda que o hospedeiro estrangeiro não possa ser obrigado a apagar o
    conteúdo ilegal de seus servidores, o acesso a qualquer provedor de conteúdo
    ilícito será prejudicado pela medida técnica implementada por provedores
    brasileiros de conexão. Por essa razão, apresentamos um projeto de lei nesse
    sentido (Parte III, 1.7).



se isso sai da subrelatoria, por quê não é específico para o tema da pornografia infantil?


Retirada de conteúdos de crimes contra a honra
==============================================

    2.3.1 – Retirada célere de conteúdos que atentem contra a honra

    Diversas são as ofensas, violações e crimes contra à
    honra da pessoa perpetrados pela internet. Também, crimes resultantes de
    discriminação ou preconceito de raça, homofobia, intolerância religiosa e outros
    atributos pessoais são objeto constante de comentários nas redes sociais. Esta
    CPI, sensibilizada ao assunto, buscou incessantemente convidar
    personalidades atacadas por essa mazela que contamina a internet. Foram
    diversos requerimentos aprovados para convidar vítimas a prestarem
    depoimento, mas todas elas declinaram por se tratar de assunto extremamente
    doloroso e pessoal. Essa recusa é um forte indicativo do grave transtorno que
    acarreta ser alvo desses comentários maliciosos e criminosos.

    Também nesta CPI foi relatado, por diversos delegados e
    procuradores, a dificuldade e a demora para se aceder à remoção de
    conteúdos ilegais. Casos não tão relevantes podem demorar 30 dias, mas
    casos extremamente danosos, que deveriam ter remoção imediata das redes
    sociais, continuaram por vários dias accessíveis aos usuários, perpetuando o
    dano às vitimas.

    Na análise da questão, verificamos que um dos
    problemas da falta de celeridade para a remoção desses conteúdos encontra-
    se nas disposições do novo Marco Civil da Internet (MCI, Lei n o 12.965/14).

    Apesar de concordarmos integralmente nos preceitos constitucionais de que a
    manifestação do pensamento deva ser livre e que os meios de comunicação
    social não devam sofrer qualquer tipo de censura, o MCI não garante a
    remoção célere de conteúdos que atentem contra a honra e que configurem
    outras injúrias. O MCI tampouco apena os responsáveis pela manutenção no ar
    dessas informações.



    2.3.2 – Retirada de conteúdos repetidos que atentem contra a honra

    Ainda nessa questão da remoção de conteúdos
    atentatórios, o estudo do assunto e as oitivas realizadas pela CPI nos indicam
    que as disposições do MCI dificultam a manutenção da exclusão de conteúdos
    criminosos quando estes são replicados. Uma vez que no texto legal (§1 o , do
    artigo 19) há a menção expressa à “localização inequívoca do material”, alguns
    dos principais aplicativos de internet tem procedido apenas a remoções de
    endereços específicos apontados em decisões judiciais. Assim, quando os
    mesmos conteúdos são postados em outra página, ou por outro usuário, os
    responsáveis pelo sítio têm exigido nova ordem judicial apontando esse novo
    endereço a ser removido. Essa prática é extremamente danosa para a vítima,
    pois as facilidades inerentes aos meios digitais, relatados anteriormente,
    tornam a prática do crime ininterrupta.

    Assim, o sistema atual é injusto com a vítima, pois as
    disposições do MCI obrigam a vitima a entrar na justiça contra cada ocorrência
    do conteúdo. Essa metodologia tem consequências nefastas para vítimas,
    pessoas comuns, celebridades, ou pessoas investidas de cargo público ou
    político.

    No caso específico do processo eleitoral, tendo em vista
    os prazos exíguos das campanhas, os ataques pelas redes sociais são
    impossíveis de serem coibidos em tempo hábil, sendo o dano eleitoral
    irreparável, com graves consequências para o processo democrático.

    Por isso, também sugerimos apresentar um Projeto de Lei
    para que essa temática seja amplamente debatida pelo Congresso Nacional.


Endereços IP como dados cadastrais
==================================

    2.3.3. – Melhoria na identificação das pessoas físicas das contas de aplicações de internet

    Esta Sub-Relatoria se debruçou sobre a sistemática de
    como as grandes empresas da internet realizam o cadastro de usuários em
    seus aplicativos. A flexibilidade, a simplicidade e a gratuidade do cadastro são
    partes indissociáveis do sucesso de todos os aplicativos, quer seja, motores de
    busca, redes sociais, correios eletrônicos ou agregadores de informações. A
    flexibilidade nesses cadastros e o sigilo dessas informações garantem a
    intimidade e a privacidade do usuário. O modelo de negócios desses
    aplicativos é de tão indiscutível sucesso que suas empresas proprietárias são
    na atualidade verdadeiros impérios globais, com faturamentos da ordem de
    bilhões de dólares. Suas ramificações brasileiras também contribuem
    largamente para a consolidação dessas verdadeiras ferramentas sociais e
    econômicas.

    Em que pese o sucesso financeiro e junto ao público,
    esses poderosos e onipresentes serviços também se prestam para o
    acobertamento de criminais, para a publicidade de serviços criminosos, para a
    comercialização de produtos ilegais e para o acometimento de crimes, de
    maneira direta ou indireta.
    Durante as oitivas verificamos que as autoridades de
    investigação muitas vezes não possuem acesso a diversas informações dos
    perpetuadores de crimes devido a facilidades da própria tecnologia – como
    mecanismos de ocultamento de IPs – mas também pela simples falta de coleta
    ou acesso a dados essenciais do internauta.

    O estudo aprofundado do assunto nos permite concluir
    que a guarda do endereço IP no momento de criação das contas possibilitaria
    às autoridades de investigação, encontrar mais facilmente autores de eventuais
    ofensas criminais. Ademais, a equiparação do endereço IP do usuário de
    internet a dado cadastral infligiria maior celeridade ao processo investigativo.

    Mediante a sistemática atual, em caso de processo de
    investigação em andamento, a autoridade judicial possui acesso aos dados
    cadastrais de qualquer usuário da telefonia. No entanto, para se obter o usuário
    que se encontra por trás de determinado endereço IP é necessário recorrer a
    mandato judicial e realizar o processo de quebra em três etapas. Primeiro junto
    ao aplicativo, segundo, junto à autoridade de registro da internet e, terceiro,
    junto à operadora de telefonia. Diversas autoridades indicaram a demora
    desses processos por diversos motivos. Desde a recusa no atendimento a
    solicitações por parte de empresas de internet que possuem suas bases de
    dados no exterior, até a falta de disponibilidade de juízes de plantão para
    emissão de ordens judiciais que autorizem a quebra do sigilo nas diversas
    etapas.

    Nesse contexto, se o endereço IP fosse tratado como
    dado cadastral, as autoridades de investigação poderiam ter acesso imediato
    ao individuo pessoa física, na maioria dos casos. A empresa responsável pela
    aplicação de internet (e pelos logs de navegação, de acordo com o Marco Civil
    da Internet) teria que informar o endereço IP, assim como a operadora de
    conexão à internet. O cruzamento imediato dessas informações permitiria a
    identificação de internautas investigados de maneira automática e imediata.

    Gostaríamos de ressaltar neste ponto do relatório que a equiparação que ora
    propomos não implica em franquear o acesso de policiais aos dados de
    qualquer internauta. Nossa proposta não altera a sistemática atual já
    prevista nas Leis 12.850/13 (Lei das Organizações Criminosas) e 9.613/98 (Lei
    da Lavagem de Dinheiro). Apenas poderão ser obtidos dados pessoais de cadastro
    para fins de investigação, isto é, com processos investigatórios já abertos.

    Qualquer uso desses dados em desacordo com esse princípio continuará sendo
    criminoso. Dessa maneira, os internautas que não tiverem acometido nenhum tipo
    de crime possuem a garantia de manutenção de sua intimidade. Já aqueles que se
    utilizarem da internet para o acometimento de crimes serão identificados
    rapidamente.  Assim, como forma de aumentar a eficácia das investigações,
    propomos **Projeto de Lei alterando a Lei das Organizações Criminosas, a Lei da
    Lavagem de Dinheiro e o Marco Civil da Internet para incluir no rol das
    informações cadastrais de usuários o endereço IP**.  (Parte III, 1.6)

(pág 185)

elaborar diff


Guarda de registros de conexão por todos os provedores de Internet
==================================================================

    2.4.2 – Guarda dos registros de conexão por todos os
    provedores de internet e migração para o IPv6

    Em diversas audiências públicas os membros desta CPI foram alertados de que
    novas modalidades de conexão à internet se utilizam de tecnologias que permitem
    o compartilhamento de endereços IPs, isto é, compartilham o mesmo número que
    identificaria de maneira única o dispositivo conectado à internet, o que
    impediria a correta identificação dos internautas.

    Nesse sentido, o Serviço de Repressão a Crimes Cibernéticos da Polícia Federal
    salienta a necessidade da guarda não apenas dos endereços IPs, mas também das
    portas utilizadas por cada usuário.

    [...]

    No aprofundamento da análise do tema, esta Sub- Relatoria pondera que a falha
    na identificação dos internautas não decorre naturalmente do uso da tecnologia
    e sim, de falha na regulamentação.

    Notadamente, a definição, pelo Marco Civil da Internet (MCI, Lei n 12.965/14),
    do que constitua provedor de internet e suas obrigações deixa um vazio legal
    para determinados tipos de provedores de conexão. O MCI dispõe que apenas os
    administradores que possuem endereços IP diretamente alocados pela autoridade
    de registro da internet no Brasil, o Cgi.br, possuem a necessidade de guardar
    registros de conexão de o seus usuários. Para esclarecimento, chamaremos esses
    provedores de primários. Normalmente provedores primários são grandes empresas
    e entidades governamentais, que gerenciam grande quantidade de usuários e de
    conexões. Dentre elas, as companhias telefônicas, do cabo e entidades Estaduais
    e Federais.

    Assim, de acordo com a Lei, provedores de conexão não primários, que por sua
    vez são usuários daqueles provedores, estão isentos da obrigação da guarda de
    registros de usuários. Como consequência, indivíduos podem acometer toda sorte
    de crimes cibernéticos quando conectados a esses provedores não primários com a
    certeza da impunidade, uma vez que seus registros de conexão não serão
    guardados.

    De maneira acertada, e afortunada para esta CPI, essa incorreção no MCI já foi
    objeto de proposição, na forma do PL 3.237/15. Por esses motivos, concluímos
    por:

        1. manifestar nosso apoio ao PL 3.237/15 que determina a guarda unívoca dos
        registros de conexão por todos os provedores de conexão; (Parte III, 5)

Fiscalização do cadastro de pré-pagos
=====================================


    2.4.5 – Fiscalização por parte do TCU das ações da Anatel no que diz respeito
    ao cadastro dos acessos pré-pagos à internet

    A perpetuação de todo crime cibernético inicia-se pelo acesso à internet.
    Assim, em se fiscalizando as formas de acesso à internet e identificando
    corretamente os usuários da grande rede é possível reduzir a ocorrência de
    crimes digitais.

ótimo raciocínio, heimmm

    O avanço da tecnologia e a massificação do uso, no entanto, estabelecem uma
    corrida ininterrupta entre malfeitores e órgãos de fiscalização e controle,
    onde os primeiros iniciam sempre em vantagem. Dentre os avanços tecnológicos e
    de mercado uma das ferramentas mais utilizadas para o acometimento de crimes
    pela internet é a utilização do pré-pago.

    Incialmente, os pré-pagos eram utilizados para práticas de extorsão para a
    compra de créditos. Atualmente, com o advento dos smartphones e das redes sem
    fio, esses telefones oferecem suporte completo para o acometimento dos mais
    variados crimes. Os trabalhos desta CPI verificaram que a compra de chips
    pré-pagos são extremamente facilitados pelas práticas das operadoras. Basta
    cadastrar um CPF fictício e é possível habilitar uma linha celular, adquirir um
    mínimo de créditos para tornar a linha operacional e navegar pela internet
    utilizando-se de redes wi-fi gratuitas apontadas para as unidades prisionais.
    Cabe salientar, ainda, que o acesso a número de celular é fundamental para a
    criação de contas e perfis nos principais aplicativos de internet, tais como
    Google e Facebook. Portanto, a correta identificação dos usuários é imperativa
    no combate aos crimes cibernéticos.

    Ocorre, no entanto, que esse cadastramento extremamente liberal por parte das
    operadoras é, na verdade, ilegal. A Lei 10.703/03 que dispõe sobre o cadastro
    de usuários de telefones pré-pagos, determina que, além do CPF, deverão constar
    do cadastro nome e endereço completos. Logicamente, para que o cadastro faça
    sentido deve haver uma conferência pelos estabelecimentos que comercializam
    esses chips, ou em última instância, pelas operadoras, para garantir a
    integridade do cadastro. Em outras palavras, se a prática comercial permite o
    uso de CPFs descasados do nome e da prova do endereço residencial, o
    procedimento equivale, na prática, ao descumprimento da Lei. Nesse caso cabe à
    Anatel agir e fiscalizar os procedimentos que estão sendo tomados pelas
    empresas em sua órbita de regulação. Em Audiência Pública nesta CPI, as
    operadoras foram unânimes em admitir a existência de falhas nesses cadastros de
    usuários do pré-pago.

    Por esses motivos, **oferecemos Proposta de Fiscalização e Controle para que,
    com auxílio do Tribunal de Contas da União, seja verificado quais procedimentos
    são tomados pela Anatel para a garantia da integridade dos dados constantes nos
    cadastros das operadoras de telefonia dos usuários da telefonia pré-paga, de
    que trata a Lei n o 10.703/03**. (Parte III, 2.1)


Notas soltas
============

Relatório

    a Comissão partiu da seguinte premissa:
    como evitar golpes e fraudes na internet.


    4. Oferecimento de Projeto de Lei permitindo o
       perdimento de bens de criminosos; (Parte III,
       1.1)

Pedro Markun na CPI:

    a Presidente da Comissão esteve lá, na última vez em que eu estive
    aqui com o Ônibus Hacker. Ela foi lá conversar conosco, a Deputada
    Mariana, e quando ela estava lá eu disse: “Olha, eu estou meio
    preocupado com essa CPI, porque toda hora que começam a falar de
    crimes cibernéticos, vocês começam a colocar um monte de coisas que
    eu não acho que são crimes”


    Então, eu fiquei meio preocupado com essa história, porque, na
    verdade, muito do que a gente está discutindo como crime cibernético
    aqui — pirataria, privacidade, publicação de conteúdo difamatório na
    Internet — ou já é crime, independentemente de ser cibernético, ou,
    mais do que isso, são mudanças culturais profundas com que a gente
    vai ter que lidar de outro jeito que não seja colocando no Código
    Penal e botando gente na cadeia.


    Eu assisto a Galinha Pintadinha no Youtube, e é muito louco, porque
    assistir a Galinha Pintadinha no Youtube significa que você vai ver
    um vídeo flipado. O que eles fazem? Eles espelham o vídeo para o
    vídeo não ser detectado pelo sistema do Youtube de detecção de
    conteúdo com direitos autorais. E ele tem 5% de aceleração. Então, o
    áudio fica um pouco mais fininho e tal, mas aí também ele burla o
    sistema de detecção do Youtube. Há outras mil manobras para burlar
    essa detecção. É isso. Eu tenho uma filha de 2 anos e outra de 4, e
    elas veem Galinha Pintadinha de vez em quando.


    o que eu gostaria de fazer hoje, mas não vou conseguir fazer no tempo
    que eu tenho, era ensinar os Deputados a usarem a Internet de
    verdade, a baixarem um uTorrent de raiz, porque, só a partir dessa
    experiência e dessa vivência — eu estou falando com muita honestidade
    —, só depois que vocês vivenciarem o que é a prática de rede, vocês
    vão ter alguma bagagem para legislar sobre isso sem necessariamente
    ferir, talvez não o direito, mas as práticas correntes de milhões de
    brasileiros. E aí, como eu não vou conseguir fazer isso em 12
    minutos, eu vou fazer um apelo para vocês passarem a todos os
    Deputados da Comissão: procurem os seus netos, sobrinhos, vizinhos,
    filhos do vizinho e, numa tarde, quando vocês voltarem para a base
    eleitoral, falem o seguinte: “Me ensina a usar a Internet”.



