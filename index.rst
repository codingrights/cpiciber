================================================
Considerações sobre a CPI de Crimes Cibernéticos
================================================

.. only:: html

    - **NOVIDADE:** `2ª Nota Técnica - Propostas para o Relatório Final da CPICIBER </propostas/>`_
        - `Versão em PDF <https://cpiciber.codingrights.org/CPICIBER_2aNotaParaParlamentares.pdf>`_
        - `Versão em PDF resumida <https://cpiciber.codingrights.org/CPICIBER_2aNotaParaParlamentares_resumida.pdf>`_

    - cpiciber.codingrights.org
        - `Todo o conteúdo em PDF <https://cpiciber.codingrights.org/CPICIBER.pdf>`_
        - `Todo o conteúdo em EPUB <https://cpiciber.codingrights.org/CPICIBER.epub>`_


****

.. toctree::
   :maxdepth: 2

   sumario-executivo
   introducao
   crimes-ciberneticos
   marco-civil
   retencao-de-dados
   criptografia
   interceptacao
   anonimato
   tor-onion
   seguranca-cibernetica
   questoes-emergentes
   creditos
   referencias
   propostas
