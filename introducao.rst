==========
Introdução
==========

As organizações da sociedade civil têm acompanhado com preocupação as sessões da Comissão Parlamentar de Inquérito de Crimes Cibernético - CPICIBER. O volume de informações e a diversidade de visões podem confundir os parlamentares e induzir a erros no momento de compreender qual a melhor legislação a ser proposta. Como forma de organizar nossa colaboração, apresentamos aqui os principais conceitos já fixados em lei, bem como os pontos em disputa.


CPI de Crimes Cibernéticos
--------------------------

A `CPI de Crimes Cibernéticos (CPICIBER) <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos>`_
foi criada em agosto de 2015, em atendimento ao requerimento apresentado
em fevereiro de 2015 pelo Deputado Sibá Machado (PT/AC), apoiado por
outros 195 deputados.

Na `proposta inicial
<http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=1304071&filename=RCP+10/2015>`_, além de enumerar diversos exemplos, três fatos
foram destacados como justificativas: (i) a Polícia Federal realizou em
2014 a operação batizada de IB2K para desarticular uma quadrilha
suspeita de desviar pela Internet mais de R$ 2 milhões de correntistas
de vários bancos, parte para comprar armas e drogas; (ii) o relatório da
Central Nacional de Denúncias de Crimes Cibernéticos apontou
crescimento, entre 2013 e 2014, de 192,93% nas denúncias envolvendo
páginas na Internet suspeitas de tráfico de pessoas, e (iii) o gasto,
informado pela Symantec, de US$ 15,3 bilhões com crimes cibernéticos no
Brasil em 2010.

Na `proposta inicial
<http://www.camara.gov.br/proposicoesWeb/prop_mostrarintegra?codteor=1304071&filename=RCP+10/2015>`_, além de enumerar diversos exemplos, três fatos
foram destacados como justificativas: (i) a Polícia Federal realizou em
2014 a operação batizada de IB2K para desarticular uma quadrilha
suspeita de desviar pela Internet mais de R$ 2 milhões de correntistas
de vários bancos, parte para comprar armas e drogas; (ii) o relatório da
Central Nacional de Denúncias de Crimes Cibernéticos apontou
crescimento, entre 2013 e 2014, de 192,93% nas denúncias envolvendo
páginas na Internet suspeitas de tráfico de pessoas, e (iii) o gasto,
informado pela Symantec, de US$ 15,3 bilhões com crimes cibernéticos no
Brasil em 2010.

Após dezenas de reuniões deliberativas e `audiências públicas
<http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos/documentos/audiencias-publicas-1>`_,
a CPI ouviu vários setores da sociedade e discutiu uma gama de outros crimes e
condutas que passam pelo uso de computadores e da Internet, como:


- Políticas de prevenção e resposta a abusos e crimes cibernéticos por empresas privadas do ramo digital como Facebook, Twitter, Google, Yahoo!, Microsoft e Whatsapp;
- Grupos e centros de resposta e tratamento de incidentes de segurança
- (CSIRT’s / CERT’s);
- Violência sexual contra crianças e adolescentes;
- Práticas abusivas de publicidade online direcionadas ao público infantil;
- Pornografia de revanche (revenge porn);
- Segurança digital das comunicações de órgãos do governo;
- Procedimentos e obstáculos no combate a crimes cibernéticos por delegados(as) de polícia, defensores(as) públicos(as) promotores(as) de justiça, analistas e outros(as) profissionais especializados na área;
- Criminalística e perícia forense de crimes cibernéticos;
- Operações policiais bem-sucedidas sobre crimes cibernéticos (como a
- Operação Barba Negra e a Operação Darknet);
- Defesa e segurança cibernética de infraestruturas críticas (bancos,
- indústria, telecomunicações, previdência social, Receita Rederal,
- sistema eleitoral);
- Proteção de consumidores contra monitoramento e uso abusivo de dados
- pessoais;
- Publicidade de empresas e setores do governo em sites de conteúdo
- ilícito, como streaming de vídeos em violação aos direitos autorais;
- Governança da Internet;
- Produção e disseminação de pornografia infantil, bem como abordagens
- policiais para repressão destes crimes, abordagens de educação e
- segurança para prevenção, abordagens psicológicas e sociais para
- tratamento de vítimas e abordagens jurídicas para responsabilização de
- criminosos;
- Depoimentos de vítimas de calúnia, racismo e crimes de ódio na Internet;
- Anonimato e pseudonimato em redes sociais e plataformas online
- (“perfis falsos / fake”);
- Terrorismo e segurança cibernética durante os Jogos Olímpicos de 2016
- e em outros Grandes Eventos;
- Limites jurídicos e técnicos do provimento de dados de aplicação por
- empresas prestadoras de serviços digitais como Whatsapp e Google;
- Ações online em defesa dos direitos das mulheres (grupo ThinkOlga);
- Legislação dos estados americanos sobre provedores de Internet
- (diligência, na embaixada dos EUA em Brasília);
- Uso de software para enganar exames de emissão de poluentes pela
- empresa Volkswagen;
- Venda de medicamentos abortivos pela Internet;


Para organizar os resultados, foram designadas quatro sub-relatorias:

1. Instituições financeiras e comércio virtual – Sub-Relator Dep. Sandro Alex (PPS/PR)
2. Crimes contra a criança e o adolescente – Sub-Relator Dep. Rafael Motta (PROS/RN)
3. Violações a direitos fundamentais e criação de perfis falsos ou satíricos com o objetivo de praticar subtração de dados, crimes contra a honra, inclusive injúrias raciais, políticas, crimes de racismo, crimes contra homossexuais, estelionato, extorsão e outros ilícitos penais, intimidação, intimidação sistemática (bullying) e referências depreciativas repetidas a determinada pessoa – Sub-Relator Dep. Daniel Coelho (PSDB/PE); e
4. Segurança cibernética no Brasil – Sub-Relator Dep. Rodrigo Martins (PSB/PI)

No `site da CPICIBER <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos>`_ na Câmara dos Deputados é possível ver o histórico de reuniões passadas e assistir gravações de áudio e vídeo das sessões.

É de se preocupar o fato de que o volume e diversidade dos temas em questão -- cujo debate demanda tanto um conhecimento extensivo sobre o funcionamento de diversas tecnologías, bem como o aprofundamento de discussões jurídicas ainda não pacíficas no Brasil e no mundo -- possa confundir os parlamentares e induzir a erros no momento de compreender qual a melhor legislação a ser proposta.

Desta maneira, a Coding Rights, em parceria com o Ibidem, como organizações com fins sociais que seguem os debates sobre a proteção dos direitos na rede, pretendendo auxiliar no trabalho desta comissão, pretendem por meio desta estudo destacar e esclarecer o que consideramos ser os principais conceitos pertinentes aos debates que se desencadearam nas audiências públicas, trazendo tanto clareza técnica quanto a perspectiva da proteção de direitos para quando se trata de segurança no ambiente virtual.

A `Coding Rights <https://codingrights.org>`_ é uma organização brasileira, criada e liderada por mulheres, com sede no Rio e em São Paulo. Dedica-se a promover a integração do entendimento e uso da tecnologia nos processos de construção de políticas públicas para avançar na garantia dos direitos humanos no mundo digital.

Contatos:

- Joana Varon, Diretora Fundadora <joana@codingrights.org> e
- Lucas Teixeira, Chief Technologist <lucas@codingrights.org>

O `IBIDEM – Instituto Beta para Internet e Democracia é uma associação sem fins lucrativos <http://ibidem.org.br/>`_, baseada em Brasília, que atua na defesa e promoção de direitos humanos no ambiente digital.

Contatos:

- Paulo Rená <paulo@ibidem.org.br>
