

Endereços IP como dados cadastrais
----------------------------------

O item 9 (item 8 na primeira versão, antes do acréscimo do PL de geolocalização) dos "principais temas tratados" nas Conclusões do Relator do relatório trata de alterar as Leis de Organização das Organizações Criminosas, a Lei de Lavagem de Dinheiro e o Marco Civil da Internet para "**incluir no rol das informações cadastrais de usuários o endereço IP**".

Como bem colocado pelo pesquisador do Ibidem, Jonathan Razen, em matéria para o `Congresso em Foco <congressoemfoco.uol.com.br/noticias/cpi-ameaca-direitos-dos-internautas/>`_ e a `Agência Pública <http://apublica.org/2016/04/truco-cpi-ameaca-direitos-dos-internautas/>`_, as propriedades de um endereço IP não combinam com o conceito de dado cadastral (grifo nosso):

    Embora represente uma localização, o IP pode ser compartilhado por mais de
    uma máquina. Para Razen, é um indício frágil de identificação do
    internauta. "**Tudo o que um IP pode fazer é indicar a região de onde veio o
    acesso. Os computadores de um mesmo gabinete podem compartilhar o mesmo IP,
    por exemplo. O IP não é um dado de uma pessoa, mas de uma máquina, e não
    serve, portanto, como dado cadastral**", diz. Assim, estaria anulada a
    presunção de inocência, continua o diretor do Ibidem, já que, em caso de
    questionamento de algum conteúdo, o usuário teria que provar que não
    utilizou determinado computador.


Além do cenário onde computadores compartilham IP em um mesmo prédio ou casa, há também um outro caso-problema já abordado nesta CPI: **muitas vezes o endereço IP de origem de uma ação na Internet não tem relação com a pessoa que a perpetrou** -- como quando se usa tunelamento por VPN para proteger a conexão em redes públicas/inseguras, ou no cenário onde o(a) criminoso(a) invade outros computadores de pessoas "normais", para deles desferir seus ataques (através, por exemplo, de  *botnets*).
