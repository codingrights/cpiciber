.. _marcocivil:

=======================
Marco Civil da Internet
=======================

O Marco Civil da Internet, Lei nº 12.965/2014, apresenta alguns conceitos e princípios específicos para a disciplina legal dos direitos e deveres nos usos da Internet no Brasil que não podem ser desconsiderados quando se trata de coibir crimes cibernéticos.

Destacamos abaixo alguns conceitos estabelecidos no Marco Civil, bem como sua aplicação na prática para melhor entendimento de como se dá o fluxo das comunicação na Internet. O Marco Civil, assim dispõe:


    | Art. 5º - Para os efeitos desta Lei, considera-se:

    | **I - internet:** o sistema constituído do conjunto de protocolos lógicos, estruturado em escala mundial para uso público e irrestrito, com a finalidade de possibilitar a comunicação de dados entre terminais por meio de diferentes redes;
    | **II - terminal:** o computador ou qualquer dispositivo que se conecte à internet;
    | **III - endereço de protocolo de internet (endereço IP):** o código atribuído a um terminal de uma rede para permitir sua identificação, definido segundo parâmetros internacionais;
    | **IV - administrador de sistema autônomo:** a pessoa física ou jurídica que administra blocos de endereço IP específicos e o respectivo sistema autônomo de roteamento, devidamente cadastrada no ente nacional responsável pelo registro e distribuição de endereços IP geograficamente referentes ao País;
    | **V - conexão à internet:** a habilitação de um terminal para envio e recebimento de pacotes de dados pela internet, mediante a atribuição ou autenticação de um endereço IP;
    | **VI - registro de conexão:** o conjunto de informações referentes à data e hora de início e término de uma conexão à internet, sua duração e o endereço IP utilizado pelo terminal para o envio e recebimento de pacotes de dados;
    | **VII - aplicações de internet:** o conjunto de funcionalidades que podem ser acessadas por meio de um terminal conectado à internet; e
    | **VIII - registros de acesso a aplicações de internet:** o conjunto de informações referentes à data e hora de uso de uma determinada aplicação de internet a partir de um determinado endereço IP.

Para ilustrar, imagine um cenário em que Maria acessa, através da Internet, uma rede social hipotética ``redesocial.com``, cujo servidor está nos EUA:

.. figure:: imagens/rede.png
    :align: center
Aqui, destacamos os elementos de acordo com a terminologia do Marco Civil:

.. figure:: imagens/rede_anotada.png
    :align: center

Para além da sessão de definições, a privacidade também aparece como princípio e direito fundamental no Marco Civil, entre seus dispositivos o texto estabelece:

    - A “proteção à privacidade” e “a proteção de dados pessoais” aparecem listadas como princípios.

    - A “inviolabilidade da intimidade e da vida privada”, bem como a “inviolabilidade e sigilo, salvo por ordem judicial, do fluxo das comunicações e das comunicações armazenadas”, aparecem como direitos assegurados.

    - O texto estabelece o direito dos usuários ter “informações claras nos contratos de prestação de serviços, com detalhamento sobre as práticas de proteção aos registros armazenados”, bem como sobre “coleta, uso, armazenamento e tratamento de dados pessoais”. E ressalta ainda que “dados pessoais apenas poderão ser utilizados para finalidades que a) justifiquem a coleta; b) não sejam vedadas pela legislação e c) estejam especificadas nos contratos ou termos de uso.”

    - Também são estabelecidos como direitos o “não fornecimento a terceiros de dados pessoais, salvo mediante consentimento livre, expresso e informado”.  E o direito à, mediante requerimento, “exclusão definitiva dos dados pessoais que tivermos fornecido a determinada aplicação de internet, ressalvadas as hipóteses de guarda obrigatória”. Trata-se, portanto, de uma previsão genérica, do direito ao esquecimento, referente apenas aos dados que o usuário cede ao provedor de aplicação (não se trata de dados publicados por terceiros) e aplicável apenas no término da relação entre as partes.

    - Por fim, o artigo 8 reconhece que a “garantia do direito à privacidade e à liberdade de expressão nas comunicações é condição para o pleno exercício do direito de acesso à internet” e, como tal, declara que serão nulas as cláusulas contratuais que impliquem na “ofensa à inviolabilidade e ao sigilo das comunicações ou que não ofereçam o foro brasileiro como opção para solução de controvérsias. [VARONeCASTANHEIRA-MCI-2014]_

Ou seja, a parte geral do Marco Civil, onde se estabelecem princípios, direitos e deveres dos usuários (Capítulos I e II), já traz referências importante ao direito à privacidade que devem ser levadas em conta como principios norteadores para qualquer atividades de combate a delitos que ocorram no meio virtual. 

Mais detalhes sobre como implementar a proteção da privacidade, tendo em vista dados cadastrais, registros de conexão e de aplicações, estão previstos no capítulo III do Marco, que trata da provisao desses dois serviços. Numa mudança de última hora, o texto aprovado passou a determinar a retenção de ampos os tipos de registros, mas estabeleceu a necessidade de ordem judicial para acessar esses dados como salvaguarda.

.. _regmci:

Regulamentação do Marco Civil da Internet
=========================================

O Marco Civil da Internet ainda não foi regulamentado; o decreto presidencial que definirá os detalhes sobre neutralidade de rede, guarda de registros de aplicação e o sistema de fiscalização para tornar efetivas as proteções da lei passou por `consulta pública <http://pensando.mj.gov.br/marcocivil/>`_, encerrada no dia 1º de março.

Os principais pontos de discussão na consulta pública dizem respeito justamente à regulamentação da proteção da privacidade na guarda de registros de conexão e aplicações e estão compilados nessa visualização de dados feita no âmbito do projeto `Oficina Antivigilância <https://antivigilancia.org/>`_:

.. figure:: imagens/antivig_dataviz.png
    :align: center
    :target: https://antivigilancia.org/pt/2015/05/privacidade-em-consulta-visualizacao-de-dados/

Um grupo de entidades da sociedade civil se reuniu para elencar entre si pontos comuns do decreto que devem necessariamente ser alvo de mudança ou de elaboração mais cuidadosa na versão final do texto. A carta circulou, foi endossada por mais grupos e apresentada como contribuição conjunta à consulta.

.. figure:: imagens/regmci.png
    :width: 576px
    :align: center

    Ilustração do ponto "Incongruências da Anatel fiscalizando aspectos da
    guarda de dados" da nota pública da sociedade civil


O texto da nota pública está disponível aqui (com ilustrações da `Oficina Antivigilância <https://antivigilancia.org>`_): `Regulamentação do Marco Civil: entenda e ajude a divulgar nossas contribuições <https://antivigilancia.org/pt/2016/03/regmci-nota-publica/>`_.
