=============
Interceptação
=============

Panorama Legal
==============

No artigo "Interceptações e Privacidade: novas tecnologias e a Constituição", o ministro do STF Gilmar Ferreira Mendes e o juiz federal Jurandi Borges Pinheiro fazem "uma análise crítica da legislação brasileira sobre interceptações telefônicas e telemáticas [...] em face dos avanços tecnológicos que se multiplicam nessa área e da garantia constitucional do direito à privacidade". [MENDES-PINHEIRO-2015]_

Os autores identificam três leis que regem a interceptação no Brasil: a `Constituição de 1988 <http://www.lexml.gov.br/urn/urn:lex:br:federal:constituicao:1988-10-05;1988>`_, a `Lei de Interceptações Telefônicas (LIT, nº 9.296/96) <http://www.lexml.gov.br/urn/urn:lex:br:federal:lei:1996-07-24;9296>`_ e o `Marco Civil da Internet (lei nº 12.965/2014) <http://www.lexml.gov.br/urn/urn:lex:br:federal:lei:2014-04-23;12965>`_.

A análise "Vigilância das Comunicações pelo Estado Brasileiro" [INTERNETLAB-2016]_ vai além destas três e nos traz também a `Lei Geral das Telecomunicações (LGT, nº 9.472/1997) <http://www.lexml.gov.br/urn/urn:lex:br:federal:lei:1997-07-16;9472>`_, que impede o acesso às comunicações e restringe o uso e a divulgação de (meta)dados de utilização do usuário pelas prestadoras de serviço de telecomunicação.

.. figure:: imagens/vigilancia_ilab.png
    :width: 400px
    :align: center
    :target: http://www.internetlab.org.br/wp-content/uploads/2016/01/ILAB_Vigilancia_Entrega_v2-1.pdf

    Vigilância das Comunicações pelo Estado Brasileiro, relatório feito  pelo centro de pesquisa `InternetLab <http://internetlab.org.br/>`_ em parceria com a `Electronic Frontier Foundation <https://www.eff.org/>`_.

Constituição de 1988
--------------------

    Art. 5º Todos são iguais perante a lei, sem distinção de qualquer
    natureza, garantindo-se aos brasileiros e aos estrangeiros residentes no
    País a inviolabilidade do direito à vida, à liberdade, à igualdade, à
    segurança e à propriedade, nos termos seguintes:

    [...]

    XII - é inviolável o sigilo da correspondência e das comunicações
    telegráficas, de dados e das comunicações telefônicas, salvo, no último
    caso, por ordem judicial, nas hipóteses e na forma que a lei estabelecer
    para fins de investigação criminal ou instrução processual penal;

Lei de Interceptações Telefônicas
---------------------------------

    Pela Lei 9.296/96, interceptações de comunicações telefônicas e de
    sistemas de informática e telemática podem ocorrer mediante ordem
    judicial, de ofício ou por requerimento de autoridade policial ou
    do Ministério Público, quando há indícios razoáveis de autoria ou
    participação em infração penal punida com pena de reclusão e
    indisponibilidade de outros meios de produção de prova (arts. 1o e
    2o). [INTERNETLAB-2016]_

Marco Civil da Internet
-----------------------

    Art. 7º O acesso à internet é essencial ao exercício da cidadania, e
    ao usuário são assegurados os seguintes direitos:

    | I - inviolabilidade da intimidade e da vida privada, sua proteção e indenização pelo dano material ou moral decorrente de sua violação;o
    | II - inviolabilidade e sigilo do fluxo de suas comunicações pela internet, salvo por ordem judicial, na forma da lei;
    | III - inviolabilidade e sigilo de suas comunicações privadas armazenadas, salvo por ordem judicial;

    [...]

.. seealso::
    :ref:`Marco Civil da Internet <marcocivil>`

Lei Geral das Telecomunicações
------------------------------


    Art. 3° O usuário de serviços de telecomunicações tem direito:

    [...]

    V - à inviolabilidade e ao segredo de sua comunicação, salvo nas hipóteses e condições constitucional e legalmente previstas;

    [...]

    IX - ao respeito de sua privacidade nos documentos de cobrança e na utilização de seus dados pessoais pela prestadora do serviço;

    [...]

    Art. 72. Apenas na execução de sua atividade, a prestadora poderá valer-se de informações relativas à utilização individual do serviço pelo usuário.

    § 1° A divulgação das informações individuais dependerá da anuência expressa e específica do usuário.

    § 2° A prestadora poderá divulgar a terceiros informações agregadas sobre o uso de seus serviços, desde que elas não permitam a identificação, direta ou indireta, do usuário, ou a violação de sua intimidade.



Comunicações em trânsito vs. comunicações armazenadas
=====================================================

O Supremo Tribunal federal, através do recurso extraordinário nº 418.416/SC, entendeu que "a proteção a que se refere o art. 5º, XII [da Constituição], é da 'comunicação de dados' e não dos dados em si considerados", ainda quando armazenados em computador [MENDES-PINHEIRO-2015]_.  No entanto esta decisão, tomada em 2010, não levou em conta a já crescente e hoje exponencialmente maior prática de gerar e armazenar em bancos de dados "na nuvem" informações que se relacionam com nossa vida de múltiplas e reveladoras maneiras.

Efetivamente, a decisão separa as comunicações em categorias; podemos chamá-las de  *comunicação em trânsito* (onde se dá a interceptação tradicional, onde se escuta o canal e grava o que trafega por ele) e *comunicação armazenada*, todas as mensagens e dados que permanecem nos servidores ou no computador dos(as) usuário(as).


Já o caso do acesso a computadores ou smartphones (onde ficam, por exemplo, registros de conversas passadas e históricos de navegação), não há legislação específica. O acesso legal se dá tradicionalmente através de operações de *busca e apreensão*, mas há uma tendência crescente preocupante de uso de *malware*, ou *cavalos de tróia*, para infectar computadores e controlá-los remotamente.

.. figure:: imagens/retencao_email.png

O ministro do STF Gilmar Mendes e o juiz federal Jurandi Borges Pinheiro, em
sua análise conjunta "Interceptações e Privacidade - Novas Tecnologias e a
Constituição", vêem com preocupação o acesso aos dados (registros, documentos, mensagens, agendas, anotações...) de usuários armazenados em servidores *online*, por violar o que a Corte Constitucional alemã chamou de "direito à integridade de sistemas de tecnologia da informação" no caso conhecido como *Online-Durchsuchungen* (grifos nossos):

    Especificamente em relação ao sigilo das comunicações, predominava
    no direito alemão, no mesmo sentido da jurisprudência do STF, há
    pouco examinada, o entendimento de que seu objeto é o conteúdo da
    comunicação no momento em que ela se realiza e não os dados
    armazenados após a sua transmissão. A razão de ser dessa distinção é
    que as informações armazenadas em qualquer dispositivo, após a sua
    transmissão, já não estariam mais expostas aos perigos que
    normalmente resultam da vulnerabilidade dos dados durante o processo
    de comunicação.

    No caso *Online-Durchsuchungen* , contudo, foi ressaltado que os
    **computadores estão presentes em todas as áreas de vida moderna** e são
    cada vez mais **essenciais ao desenvolvimento da personalidade**.
    Todavia, enquanto criam novas oportunidades, também colocam em **risco**
    os seus usuários. Dessa forma, **tendo em conta a possibilidade de
    busca e apreensão remota de dados já armazenados, sem a necessidade,
    portanto, da apreensão do computador, a proteção com base na
    distinção entre transmissão e armazenamento passou a se mostrar
    insuficiente**.

    Diante dessa nova realidade, adotou o Tribunal o conceito de
    *sistema de tecnologia da informação* como um sistema com capacidade
    de conter dados técnicos a um ponto que fosse possível ter
    conhecimento de uma substancial parcela da vida de um indivíduo e
    noção significativa de sua personalidade. Com base nesse conceito,
    assentou a Corte que a confidencialidade e **a integralidade dos
    sistemas de tecnologia da informação configuram direito fundamental
    comparável à inviolabilidade do domicílio**.

    [...]

    Ressaltou a Corte, todavia, que "busca *on-line*" de informações
    poderia, nesses casos, ser **justificada com a finalidade de prevenir
    práticas criminosas, mediante autorização judicial** e desde que
    observados os **requisitos constitucionais** de clareza e determinação
    jurídica.

    [...]

    Essa nova concepção do direito à privacidade, sem precedente no
    direito alemão, **serviu como forma de preencher uma lacuna até então
    existente e passou a ser um marco não apenas na Alemanha, mas em
    toda a Europa**. [MENDES-PINHEIRO-2015]_


Embora o Marco Civil da Internet coloque limites -- principalmente após ser
regulamentado -- no acesso aos registros de conexão e aplicação, os requisitos
para acesso ainda não estão tão claros quanto necessários para um artifício tão
intrusivo, e a possibilidade de acesso aos dados armazenados "na nuvem" (que
não os registros) está em uma zona de insegurança jurídica, como apontam também
Gilmar Mendes e Jurandi Borges Pinheiro (grifos nossos):

    [após citar e explicar os trechos do Marco Civil que tratam de guarda de
    registros:] O citado diploma legal assegura aos usuários da internet,
    expressamente, não apenas o sigilo do fluxo das comunicações, já objeto de
    regulamentação pela Lei n. 9.296/96, como também a inviolabilidade e o
    sigilo dos dados armazenados (art. 7º, II e III), os quais somente podem
    ser disponibilizados mediante ordem judicial (art. 10, § 2º).

    [...]

    Como se percebe, contamos, agora, com um conjunto de dispositivos
    legais que busca proteger, com razoável detalhamento, as operações
    de coleta e armazenamento de dados, os registros de conexão, bem
    como os conteúdos acessados, baixados ou transmitidos. Cabe
    destacar, como ponto positivo entre as medidas adotadas, a expressa
    exigência de autorização judicial para qualquer forma de acesso a
    esses dados, preservando-se, com isso, a privacidade do usuário.

    [...]

    Cabe ressalvar, contudo, a **insuficiente proteção da nova lei por não
    especificar os requisitos a serem observados na autorização judicial
    de acesso aos dados armazenados, que podem abranger, conforme há
    pouco se destacou, não apenas dados obtidos pela Internet, como,
    também, arquivos gerados e mantidos em pastas locais sem conexão com
    aplicativos on-line**. [MENDES-PINHEIRO-2015]_


Seria interessante, então, ter como um dos encaminhamentos da CPI uma melhor regulamentação ou doutrina jurídica sobre a "interceptação" de comunicações e dados armazenados *online*, além dos registros de conexão e aplicação já tratados pelo Marco Civil da Internet, bem como um esclarecimento sobre os contextos ou as autoridades que podem requisitar acesso aos registros dos provedores de conexão e aplicação. O ideal é caminhar em direção a um direito semelhante ao de "integralidade dos sistemas de tecnologia da informação" da Alemanha.


.. seealso::
    * :ref:`Retenção de Registros de Conexão e Aplicações <retencao>`
        * :ref:`Violação de direitos? <retencao_violacao>`
        * :ref:`Metadados <metadados>`

    * :ref:`Marco Civil da Internet <marcocivil>`
        * :ref:`Regulamentação do Marco Civil da Internet <regmci>`
