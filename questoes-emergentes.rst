===================
Questões Emergentes
===================

.. _deepweb:

Deep Web
========

A *deep web*, um conceito que é muito confundido e virou sinônimo da *rede
onion* (ver subseção abaixo), não é de relevância particular no contexto desta
CPI -- seus sites operam sob as mesmas regras e restrições dos que se pode
encontrar em buscadores como o Google. Como a apropriação já está em uso (na
própria comissão e no vocabulário popular, cabe reforçar este fato: **a
deep web joga sob as mesmas regras técnicas e jurídicas que os outros sites da
web**.

A deep web compreende todas as páginas web (ou seja, tudo que pode ser acessado
pelo navegador) que não estão indexadas e catalogadas nos grandes mecanismos de
busca, como Google, Bing e DuckDuckGo. O termo foi inicialmente cunhado para
representar a grande parcela da web que não pode ser encontrada nestes portais,
indicando que a web é muito maior do que parece à primeira vista -- daí a
metáfora comum da web "superficial" como a ponta de um iceberg.

Já o termo *dark web* significa a rede de sites acessíveis através de
softwares específicos (geralmente o Tor) que *não revelam seu local
geográfico* para quem as acessam, *não podem ser bloqueados* e cuja interação
com seus usuários é *tecnicamente inviolável*.

.. seealso::
    :ref:`Tor e Rede Onion <tor>`

Como estas características impedem alguns procedimentos comuns de policiais e
juízes, ela por diversas vezes foi trazida à CPI como um problema a ser
combatido ou criminalizado, mas nós acreditamos que os obstáculos
investigativos devem ser avaliados em contraste com seu amplo uso como
ferramenta para a garantia da liberdade de expressão, como na proteção contra
monitoramento *online* abusivo, espionagem industrial ou internacional, no
combate à corrupção e recebimento de denúncias anônimas, na instrumentalização
do sigilo da fonte jornalística, entre outros fins protegidos pela nossa
própria Constituição, por tratados e organizações internacionais.

.. seealso::
    :ref:`Anonimato Online <anonimato>`

Justamente por causar confusão quanto ao seu objetivo por lembrar "escuridão",
o termo "dark web" está em desuso; e o nome dado pelo Projeto Tor, pela
comunidade que promove e trabalha no projeto e pela mídia especializada é
**onion web**. Assim como a web "tradicional" é uma parte da Internet, a *onion
web* é parte da *rede onion*.


.. figure:: imagens/iceberg.png
    :align: center

    Esta representação transforma a representação usual da deep web, com a web superficial na ponta, em contraste com a *onion web*, que dentro da mesma metáfora deve ser representada como um iceberg à parte, com sua própria "*onion web* superficial", indexada em sites como o `Ahmia <https://ahmia.fi/>`_, e a sua "*deep onion web*". **A proporção entre os icebergs é ilustrativa; numa escala real, o iceberg *onion* ficaria menor que um pixel** (todas as estimativas apontam para menos de 50 mil sites *onion*, versus os 900 milhões de sites na web aberta `contabilizados <http://news.netcraft.com/archives/category/web-server-survey/>`_ em dezembro de 2015 pela Netcraft).

.. figure:: imagens/redeonion-comparada.png
    :align: center

.. figure:: imagens/redeonion-comparada2.png
    :align: center

    Representação da "onion web" e da web tradicional, na real proporção de acordo com os mesmos números (50 mil websites onion, um teto generoso, versus 900 milhões de sites na web de acordo com a Netcraft -- 18.000x mais)


****

======================================  ==================  =========  ========  =========  ===============
É possível...                           Web "superficial"   Web + Tor  Deep Web  Onion Web  Deep Onion Web 
======================================  ==================  =========  ========  =========  ===============
Buscar sites nos serviços tradicionais  **Sim**             **Sim**    *Não*     *Não*       *Não*
--------------------------------------  ------------------  ---------  --------  ---------  ---------------
Buscar com sistemas especializados      **Sim**              **Sim**   Depende*   **Sim**     Depende*
--------------------------------------  ------------------  ---------  --------  ---------  ---------------
Localizar usuário(a)                    **Sim**             *Não*      **Sim**   *Não*       *Não*
--------------------------------------  ------------------  ---------  --------  ---------  ---------------
Localizar servidor                      **Sim**             **Sim**    **Sim**   *Não*       *Não*
======================================  ==================  =========  ========  =========  ===============

\* Páginas que os buscadores tradicionais meramente não se deram o trabalho de processar e indexar sim; páginas que precisam de *login* para serem acessadas, não.


.. _pornografia-infantil:
Pornografia Infantil
====================


Uma das preocupações específicas manifestada nesta CPI sobre a rede *onion* é o seu uso para envio de pornografia infantil.

Em recente conversa organizada pelo Instituto Educadigital com o tema "por uma internet mais positiva", o diretor de educação da Safernet Rodrigo Nejm disse que o Brasil "não é considerado um país produtor de pornografia infantil":

    A gente [da Safernet] sempre disse isso mas agora a gente vê que até mesmo
    na imprensa isso está mais claro, e mais fraco este discurso de que no
    Brasil o maior problema é a pornografia infantil na Internet [...] é muito
    bom que a gente possa ampliar a pauta e passar pra uma discussão mais além
    do pânico moral.

A Safernet, que já foi representada na CPICIBER por Thiago Tavares, presidente da organização, trabalha no combate à pornografia infantil e vários outros crimes através de redes internacionais como a INHOPE, a InSafe e a Child Helpline International. 


.. figure:: imagens/safernet_denuncias.png
    :align: center

    Organograma do funcionamento da Central Nacional de Denúncias de Crimes Cibernéticos, que coordena a Safernet com a Secretaria Especial de Direitos Humanos, a PF e o MPF, provedores de conexão e aplicação, e organizações de cooperação internacional. Apresentado por Thiago Tavares na CPICIBER.


.. figure:: imagens/inhope_tophosting.png
    :align: center
    
    Infográfico da INHOPE mostrando a localização dos principais provedores de
    pornografia infantil. Apresentado por Thiago Tavares na CPICIBER.


Além de receber e encaminhar denúncias, o trabalho da Safernet é também muito focado na educação de crianças, adolescentes e o público em geral no uso da Internet para que os abusos e crimes nem cheguem a acontecer -- e ferramentas de anonimato são parte dos temas abordados. Conforme conta Rodrigo Nejm:

    [A SaferNet produziu] um guia chamado "`Internet Que Queremos
    <http://new.netica.org.br/adolescentes/cartilhas>`_", que é feito para
    adolescentes discutirem rastros digitais, privacidade, liberdade de
    expressão, anonimato, saber a importância do anonimato, diferenciar ele de
    outras situações e assim por diante. [...] A ideia é de uma vez por todas
    superar o pânico moral e investir pesado nessa educação para a cidadania
    digital.

Em um  `painel do re:publica 2015 <https://re-publica.de/session/deeper-frontier-freedom-state-deepweb>`_ sobre a rede *onion*, o oficial de polícia alemão Heiko Rittelmeier perguntou ao pesquisador de segurança e membro do Projeto Tor Jacob Appelbaum sobre a possibilidade de banir sites de pornografia infantil da rede. Jacob explicou como, além de inevitavelmente :ref:`ferir a liberdade de conexão <liberdade_de_conexao>` e pôr vários outros conteúdos em risco, combater desesperadamente os sintomas não ajudará a tratar as causas da doença (tradução e grifos nossos):

    O problema com a pornografia infantil não é que se pode vê-la e que
    agora há evidência dela. O fato de que alguém pode ver que há uma
    epidemia de pessoas causando danos a crianças é obviamente um problema.
    Mas não se pode ignorar que tal epidemia existe. E é melhor que a
    polícia tenha acesso a isso para que tenha evidências e possa de fato
    seguir o rastro dessas pessoas e ir até a raiz do problema.

    **E qual é a raiz do problema? Não é o anonimato. O problema é que
    pessoas estão estuprando e abusando de crianças. Da mesma maneira
    que a câmera polaroid não era o problema, o Tor também não é. Esta é
    problema sistêmico da sociedade, e para resolvê-lo é necessária cooperação
    internacional.** [...] Se você esconde [o problema], você não resolve a
    causa sistêmica. Se você o expõe, então sabe que precisa procurar pelas
    raízes sistêmicas e lutar contra elas radicalmente. Talvez você tenha que
    tomar ações drásticas, e talvez alguns criminosos, algumas vezes, escapem.
    Mas com certeza será impossível ignorar o cerne, o problema verdadeiro. 

    Muitas pessoas dirão que "é um problema que se possa ver este tipo de
    coisa", ou que "é um problema que isto esteja sendo compartilhado". E *é*, mas
    é um problema menor se comparado ao das pessoas que estão fazendo mal a
    crianças de verdade; então lhe pergunto: "*você não pensa nas crianças?*"

    Nós geralmente ouvimos isso na direção oposta: "vamos nos livrar do
    anonimato porque... pensem nas crianças!", mas **se você remover o
    anonimato, você não vai estar ajudando as crianças de verdade, e de
    fato vai estar retirando delas uma das únicas ferramentas disponíveis
    gratuitamente para as crianças se protegerem na Internet**.
    
    [...] Temos que enfrentar o cerne do problema; **o abuso de crianças é um
    problema social, e não vamos resolvê-lo tecnologicamente**, mas
    socialmente, e temos que fazer isto em escala global. Isso significa
    que **temos que respeitar os direitos das crianças, e parte destes
    direitos, [no caso de comprometer o Tor], estão muito claramente sendo
    violados**. Então vamos trabalhar em encontrá-los mas não comprometendo o
    sitema de anonimato.



.. _invasao:

Invasão de computadores
=======================

Além do acesso a dados armazenados em provedores através de ordens judiciais, outra tática que está se tornando popular entre órgãos investigativos do mundo todo conforme a adoção de criptografia aumenta nas ferramentas de comunicação *online* é a **invasão de computadores** através de **códigos de exploração de vulnerabilidades, ou exploits**. A invasão pode ser feita uma única vez, para acessar documentos e registros, ou pode instalar um **programa espião** / **cavalo de tróia** que registra conversas e ações e as manda de volta para seu "dono" através da rede ou em uma subsequente visita ao alvo.

.. figure:: imagens/exploit.png
    :align: center
    :width: 400px

****

.. figure:: imagens/exploit2.png
    :align: center
    :width: 400px


Vulnerabilidades de software
----------------------------

Quando usamos um computador ou smartphone para navegar na Internet, ler e escrever documentos, trocar mensagens ou ouvir música, o que é exibido na tela é somente a ponta de um gigantesco iceberg -- por baixo dos aplicativos abertos e da área de trabalho, milhares de *softwares* feitos de forma independente por organizações e indivíduos ao redor do mundo.

Cada um desses programas se comunica com os outros através de mensagens, encaminhadas pelo *kernel*, o núcleo do sistema, e através da Internet quando se está usando uma aplicação na rede. Quando recebe uma dessas mensagens, um programa extrai o conteúdo dela e "encaixa" em seu algoritmo para decidir o que fazer com isso.

Uma **vulnerabilidade de software** existe quando se pode mandar uma mensagem construída especialmente para confundir o sistema que se quer invadir ("batizada", como diz a expressão popular) e fazê-lo encarar as mensagens recebidas como ordens de comando, ou conceder acesso privilegiado a determinado usuário que já existe no sistema.

Um **exploit**, ou **código de exploração**, é um programa que explora uma vulnerabilidade para conseguir na prática tal acesso ao sistema.

Por serem usadas para cometer crimes -- seja fraudes bancárias, roubo de segredos industriais e dados pessoais, falsidade ideológica, fraude, etc -- as vulnerabilidades não são desejadas pelos fabricantes de software, que possuem equipes dedicadas a encontrá-los -- seja por conta própria, por relatos de incidentes ou através de sistemas de recompensa a pesquisadores -- e resolvê-los, mudando o código para remendar a falha e encaminhando atualizações de segurança para os usuários.

O mundo da segurança da informação e dos cibercrimes (e, como vemos, da "interceptação legal") é uma eterna caça de gato e rato entre quem pesquisa vulnerabilidades e quem as conserta, um determinado *exploit* descoberto ou adquirido possui prazo de validade. Vulnerabilidades, quando mantidas por muito tempo em segredo, são descobertas independentemente por outros(as) pesquisadores(as), e quando adquiridas costumam ser vendidas também para outras entidades; uma vez que se dê o uso da vulnerabilidade para invasões se torna questão de tempo até que as empresas de antivírus e segurança da informação detectem a falha e corrijam o *software*.

0-days: corrida contra o tempo
------------------------------

As vulnerabilidades com status de **0-days / zero-days** ("zero dias") são aquelas que ainda não foram descobertas por profissinais de segurança e não ainda não foram usadas contra nenhum alvo (ou estão começando exatamente agora). O nome é uma referência à contagem de dias que levam até que o problema seja corrigido, os possíveis alvos atualizem seus sistemas e a falha não possa mais ser usada tão eficazmente.

Como os *softwares* que usamos vêm tornando sua rotina de atualização mais eficiente e automática, é cada vez mais difícil usar uma vulnerabilidade já "gasta" em novos alvos. A busca por *0-days* se torna necessária para qualquer operação importante, e consegui-los envolve interagir com um mercado que se encontra, no mínimo, em uma zona cinzenta de legalidade.

Há uma divisão moral clássica nos profissionais de segurança da informação: os *whitehats* ("chapéus brancos") são aqueles que pesquisam, descobrem e catalogam vulnerabilidades e vírus para que as empresas antivírus e as fabricantes de software possam consertá-los, e os *blackhats* ("chapéus pretos") usam as falhas que descobrem ou aprendem com outros(as) *blackhats* para cometer fraudes ou vender serviços de invasão e roubo de dados por encomenda. Profissionais de diferentes chapéus e intenções interagem através de fóruns da Internet e a linha que separa a pesquisa responsável da atividade criminosa por vezes não é clara -- há um terceiro grupo, os *grayhats* ("chapéus cinzas") que não se encaixam ou rejeitam a classificação clássica.

Um relatório de 2014 da norteamericana RAND Corporation sobre o mercado negro de vulnerabilidades e serviços de invasão afirma que o preço de um único *0-day* "varia de alguns milhares de dólares a US$200.000,00 - US$300.00,00, dependendo do grau de severidade da vulnerabilidade, da complexidade do *exploit*, por quanto tempo a vulnerabilidade permanecerá desconhecida, a versão do produto que será comprometido, e o(a) comprador(a). Algumas estimativas chegam até US$ 1 milhão, mas costumam ser tachadas de exageradas" [RAND-VULNS-2014]_.

Vulnerabilidades já conhecidas, que como vimos são menos eficazes, são mais facilmente encontráveis em fóruns e listas de discussão de segurança, e até em mercado como o ExploitHub, que comercializa *exploits* de vulnerabilidades que não são *0-day*. De acordo com matéria do portal de notícias ZDnet e relatórios financeiros da ExploitHub, em 2013 o valor médio de um *exploit* era de US$ 284,06; o valor chega até US$ 1.500,00 [ZDNET-VULNS-2014]_.

Legalidade incerta; riscos garantidos
-------------------------------------

Uma vez invadido o sistema, as comunicações visadas pela "interceptação" são reveladas junto com fotos, documentos e registros pessoais, e outras comunicações que nada têm a ver com o propósito da investigação. Por se tratar de uma operação feita sem a cooperação de provedores, atravessando todos os mecanismos de autenticação do sistema invadido, não há uma maneira clara de identificar as ações do(a) invasor(a) nem de limitá-las a algum escopo; de fato, não há como diferenciar o acesso legal que partiu de uma ordem judicial de uma invasão criminosa para fraude ou criação de *botnets*.

Como diz Laura Schertel Mendes, coordenadora do Centro de Direito, Internet e Sociedade – CEDIS-EDB/IDP (grifo nosso):

    Se o STF no RE 418.416/SC já entendeu que a garantia da
    inviolabilidade de sigilo art. 5°, XII, referia-se à comunicação
    de dados e não aos dados em si, é porque certamente o cenário dos
    riscos ao cidadão era bastante diverso tendo em vista as
    tecnologias então existentes.  Afinal, usualmente os dados sofrem
    maior risco de interceptação durante o processo de comunicação –
    isto é, no tráfego – e não enquanto eles estão armazenados. Ocorre
    que com o advento da internet e dos aparelhos pessoais conectados
    em rede, a constelação de riscos alterou-se radicalmente e os
    **programas espiões** são o maior exemplo do **risco de acesso clandestino**
    e de manipulação dos dados armazenados em sistemas
    pessoais, que na vida moderna, guardam praticamente todas as
    informações a respeito de seu usuário. Nesse contexto, **a
    efetividade da garantia constitucional da inviolabilidade do
    sigilo pressupõe que ela alcance também os dados armazenados em
    sistemas informáticos pessoais – tais como computadores,
    smartphones e agendas eletrônicas – cujo acesso passa a ser
    possível por meio desses programas e que podem acarretar riscos
    gravíssimos de monitoramento e vigilância ao cidadão sem que ele
    tome sequer conhecimento a respeito**. [MENDES-2015]_

Em sua análise "Interceptações e Privacidade", o ministro do STF Gilmar Mendes e o juiz federal Jurandi Borges Pinheiro também vêem com preocupação a utilização indiscriminada de tecnologias de invasão para acessar dados e comunicações, prática ainda não bem representada no arcabouço legal brasileiro, possa causar danos ao ferir o direito à privacidade (grifos nossos):

    [...] não é porque eventual inovação no campo tecnológico não esteja
    suficientemente contemplada na legislação em vigor que a garantia
    constitucional ameaçada fica sem proteção, cabendo ao intérprete, ao lidar
    com essa realidade, assegurar que o direito fundamental em si,
    independentemente das garantias a ele inerentes, não seja menosprezado a
    ponto de negar-lhe efetividade.  Talvez seja esse o caminho ao lidarmos com
    a proteção do direito à privacidade, quando fragilizado por tecnologias que
    se transmudam da ficção à realidade em velocidade sem precedentes.

    Com essas considerações, poderíamos avançar em relação ao tema não
    mais nos preocupando tanto em contemplar, em textos legais, de modo
    específico, cada nova tecnologia que surge, mas, sim, com a
    **reformulação dos modelos de regulação, de forma a estabelecer
    requisitos mínimos como, por exemplo, crimes passíveis de
    investigação por tecnologias invasivas, imprescindibilidade de
    autorização judicial, duração da investigação, forma de registro de
    dados obtidos, restrições na divulgação dos dados capturados e
    sistema de acompanhamento do efetivo cumprimento dos requisitos
    estabelecidos**.

    Enfim, seja qual for o cenário tecnológico que nos cerca, não se
    pode perder de vista que a boa aplicação dos direitos fundamentais
    de caráter processual, principalmente da proteção judicial efetiva,
    é que nos permite **distinguir o Estado de Direito do Estado Policial**.
    O prestígio desses direitos configura também **elemento essencial de
    realização do princípio da dignidade humana na ordem jurídica**,
    impedindo, dessa forma, que o homem seja convertido em mero objeto
    do processo.

