.. _anonimato:

================
Anonimato Online
================

.. figure:: imagens/anonimizacao.png
    :align: center

Vedação constitucional
======================

**Liberdades de manifestação vs. acesso à informação**

Diferentemente de outros países democráticos, a Constituição no Brasil
proíbe expressamente o anonimato, mantendo previsão existente desde
1891. Entretanto, o texto de 1988 veda o anonimato exclusivamente no
âmbito da manifestação do pensamento.

Portanto, **não há nenhuma proibição de que haja anonimato no acesso à
informação, prática corrente não só na Internet, mas também na vida
presencial**: ninguém precisa se identificar para ir o teatro ou ao
cinema, visitar um museu ou para comprar um livro, jornal ou revista.

Em segundo lugar, precisamos lembrar de situações em que o anonimato se
coloca excepcionalmente como uma **proteção necessária para que críticas
ou denúncias possam ser feitas**, seja para permitir o combate a crimes
contra indivíduos, seja para viabilizar o combate a violações de
direitos coletivos de grupos ou de toda a população.

Finalmente, é importante **separar conceitualmente o anonimato da prática
de crimes em si**, até porque é normal que crimes não exijam a
identificação prévia de seus autores. Para muitos casos, a simples
tipificação da conduta já inclui a ocultação da identidade do agente;
para mais casos ainda, a exigência de identificação pode fragilizar a
liberdade de expressão lícita, em todos os casos em que as pessoas se
sintam vigiadas e, por isso, receosas em compartilhar suas histórias.
Não por acaso, diversos artistas ao longo de toda a história se
utilizaram de **pseudônimos e nomes coletivos para proteger sua
identidade sem abrir mão de sua expressão cultural**.

Usos legais do anonimato no Brasil
==================================

**As duas principais formas de proteção ao anonimato no Brasil são na proteção
do sigilo da fonte jornalística e no uso difundido de denúncias anônimas
em investigações do poder público.**

Sigilo da fonte jornalística
----------------------------

O professor Walter Capanema, advogado e professor da Escola de Magistratura do
Estado do Rio de Janeiro, cita os professores Vicente Paulo e Marco
Alexandrino, em sua obra Direito Constitucional Descomplicado, para concluir
que "não há conflito entre o art. 5o, IV, CF com a norma constitucional que
garante o sigilo da fonte da informação (art. 5o, XIV: 'é assegurado a todos o
acesso à informação e resguardado o sigilo da fonte, quando necessário ao
exercício profissional')" [CAPANEMA-2012]_:

    Note-se que a garantia do sigilo da fonte não conflita com a vedação ao
    anonimato. O jornalista (ou o profissional que trabalhe com a divulgação
    de informações) veiculará a notícia em seu nome, e está sujeito a
    responder pelos eventuais danos indevidos que ela cause. Assim, embora a
    fonte possa ser sigilosa, a divulgação da informação não será feita de
    forma anônima, de tal sorte que não se frustra a eventual
    responsabilização de quem a tenha veiculado – e a finalidade da vedação
    ao anonimato é exatamente possibilitar a responsabilização da pessoa que
    ocasione danos em decorrência de manifestações indevidas. [PAULOeALEXANDRINO-DIREITO-2007]_

A advogada e especialista em direito penal Tatiana Moraes Cosate, em seu artigo
"Liberdade de informação e sigilo da fonte", estabelece que "sem as fontes,
seria praticamente impossível transmitir qualquer tipo de informação ao
público, pois são elas que subministram os fatos e as informações ao repórter,
sendo imprescindíveis na realização do trabalho jornalístico" [COSATE-2009]_.

No quarto capítulo, "Aspectos jurídicos do sigilo da fonte no Brasil", após
lembrar decisões judiciais e posições de magistrados, Cosate conclui que "o
entendimento pátrio é de que o sigilo da fonte configura-se como um direito
absoluto, não existindo, no ordenamento jurídico, qualquer restrição ao uso
desse direito".

Cosate cita a decisão do ministro Celso Mello no Supremo Tribunal Federal, em
1996, que "além de conferir ao jornalista o direito de não relatar a sua fonte
de informação ou a pessoa de seu informante em juízo, ela assegura e
desautoriza qualquer medida tendente a pressionar ou a constranger o
profissional da Imprensa a indicar a origem das informações a que teve acesso"[COSATE-2009]_:

    O ordenamento positivo brasileiro, na disciplina específica desse tema (Lei
    nº 5.250/67, art. 71), prescreve que nenhum jornalista poderá ser compelido
    a indicar o nome de seu informante ou a fonte de suas informações. Mais do
    que isso, esse profissional, ao exercer a prerrogativa em questão, não
    poderá sofrer qualquer sanção, direta ou indireta, motivada por seu
    silêncio ou por sua legítima recusa em responder às indagações que lhe
    sejam eventualmente dirigidas com o objetivo de romper o sigilo da fonte
    (...).

    Eis que - não custa insistir - os jornalistas, em tema de sigilo da
    fonte, não se expõem ao poder de indagação do Estado ou de seus agentes
    e não podem sofrer, por isso mesmo, em função do exercício dessa
    legítima prerrogativa constitucional, a imposição de qualquer sanção
    penal, civil ou administrativa." [MELLO-STF-1996]_

Em outubro de 2015, então decano do STF, o ministro Celso Mello afirmou que
o sigilo da fonte é o "meio essencial de plena realização do direito
constitucional de informar", e "instrumento de concretização da própria
liberdade de informação", em uma Reclamação ajuizada contra decisão de retirar
uma reportagem do ar por conter fontes *em off* (no jargão jornalístico, sem
serem identificadas). [CONJUR-2016]_

.. _denuncias:

Denúncias anônimas
------------------

Já no que se trata das denúncias anônimas, o professor Walter Capanema
sustenta que a jurisprudência do Supremo Tribunal Federal admite a
validade das denúncias "quando tais documentos forem produzidos pelo
acusado, constituírem o corpo de delito, ou, ainda, quando a referida
denúncia anônima for precedida de uma investigação para atestar a sua
veracidade". Capanema continua: "Há julgados, inclusive, que ressaltam a
importância da investigação policial deflagrada por denúncia anônima,
pois o manto do anonimato tem servido como instrumento para a divulgação
de condutas criminosas, especialmente através dos sistemas de
'disque-denúncia'." [CAPANEMA-2012]_

O Disque Denúncia foi concebido no Rio de Janeiro em 1995, e registra mais de
2 milhões de denúncias acumuladas no estado desde então. A partir da
experiência fluminense, o Disque Denúncia foi reproduzido em todos os estados e
no distrito federal, e hoje conta com frentes especializadas em crimes
ambientais, violência doméstica, criminosos foragidos e localização de pessoas
desaparecidas.

Uma das missões é "manter a credibilidade e garantir a segurança ao seu
denunciante através do anonimato" [DISQUEDENUNCIA]_. Segundo Zeca
Borges, enquanto coordenador do serviço no Rio, em Recife e Campinas,
falando para matéria do G1, a população prefere a denúncia anônima
"porque teme um envolvimento com o caso". [G1-2007]_

Ao longo dos últimos anos, órgãos públicos têm criado plataformas de denúncia online, como o `NightAngel <http://nightangel.dpf.gov.br/>`_ da Polícia Federal e o `Web Denúncia <http://www.webdenuncia.org.br/>`_, da Secretaria de Segurança Pública de São Paulo.

.. seealso::
    :ref:`Tor e Rede Onion > Rede Onion e Onion Web > Uma garantia técnica do anonimato <garantia-de-anonimato>`


Espaços anônimos e espaços vigiados
===================================

.. figure:: imagens/vigiados.png
    :align: center

**A Internet, por princípio e por construção coletiva, permite e encoraja o anonimato. Conforme a rede é adotada por processos comerciais e governamentais, é necessário garantir a segurança e a integridade destas comunicações e atividades online. As leis, regulamentações, padrões e protocolos relativos à rede devem preservar a possibilidade de criar e compartilhar conteúdo de forma aberta, irrestrita e sem permissão, tornando seguros, registrados e identificados tão somente as interações que concernem tais espaços comerciais ou governamentais.**

****

A Constituição Brasileira de 1988, e seu art. 5º, veda o anonimato ao garantir a liberdade de expressão. O que essa vedação representa no meio digital, no entanto,
se torna incerto à medida em que as tecnologias de informação e
comunicação se integram às nossas atividades diárias, públicas e privadas, sem
trazer consigo algumas garantias e direitos fundamentais que conquistamos na
vida *offline*.

Em um passado não tão distante, todas as ações eram intrinsecamente *anônimas*
-- um grupo de pessoas conversando ou trabalhando juntas sabe quem falou e fez o
quê, mas nenhum tipo de investida policial poderia descobrir, quiçá comprovar
perante um tribunal, o que foi dito ou feito sem a cooperação de alguém de
dentro ou algum tipo de mecanismo extremamente invasivo de
vigilância (como uma escuta, infiltração ou patrulha policial). Toda ação deixa vestígios,
mas para encontrá-los e entendê-los é necessário o trabalho especializado de
detetives, e uma suspeita muito bem fundamentada de que algo errado havia
ocorrido ali. Quase tudo o que acontecia ficava, a princípio, entre as
testemunhas oculares.

Conforme as cidades cresceram em tamanho e população, instituições como bancos,
cartórios, mercados, correios, fábricas e o próprio governo em uma posição
vulnerável, ao ter que realizar transações sensíveis ou valiosas com pessoas
que seus funcionários não conheciam pessoalmente.

Para estabelecer vínculos de confiança e oferecer defesa contra abuso e crimes
foram criados vários mecanismos de identificação: cadastros de pessoas físicas
e jurídicas, serviços de proteção aos comerciantes, bancos de dados bancários e
de crédito, e outras formas das instituições coletarem assinaturas, números
de identificação e impressões digitais e poderem trocar ou conferir estas
informações entre si.

A coleta de dados sobre indivíduos como etapa obrigatória para determinadas
interações com clientes e cidadãos, serve tanto como meio de levar uma pessoa
infratora à justiça quanto identificar que uma pessoa, vinda anônima das ruas,
não é uma é uma impostora e sim a pessoa titular de uma conta bancária ou destinatária de uma encomenda.

Por princípio, a Internet reflete e amplifica o anonimato presente nas
ruas e espaços públicos da cidade. Nos *chats*, fóruns e *blogs* e em
muitas redes sociais, é possível se expressar e interagir a partir de um
pseudônimo, um *nickname*, e facilmente trocá-lo para adquirir uma nova
"identidade".

Conforme a Internet passa rapidamente de ferramenta de pesquisa e
comunicação casual a parte integrada e dissociável da vida pública, das
interações das pessoas com empresas e serviços governamentais, se torna
ferramenta para roubar ou extorquir dinheiro, cometer estelionato e
causar outros tipos de prejuízo. Com isso, torna-se necessário
identificar, e possivelmente registrar, as atividades de uma pessoa
dentro destes espaços sensíveis.

No entanto, a possibilidade de interação *online* de forma anônima e
pseudônima fora destes espaços de apropriação da Internet pelos setores
comerciais e governamentais que necessitam de controle e identificação
é parte da concepção dos protocolos da rede, permitiu o fortalecimento
de movimentos sociais e de uma maior participação política pelos
cidadãos e cidadãs, fez surgirem novas formas de interação social, deu
voz a incontáveis indivíduos e grupos marginalizados pelos meios de
comunicação tradicionais, e sem dúvida foi um grande estímulo às
inovações que *startups*, ONG's e pequenos empreendedores têm
desenvolvido através da Internet, com notável participação do Brasil.

Nas palavras de David Kaye em relatório sobre criptografia e anonimato para as
Nações Unidas: "anonimato online fornece a indivíduos e grupos uma zona de
privacidade para manter opiniões e exercitar a liberdade de expressão sem
interferência ou ataques arbitrários ou ilegais".


Proteção contra monitoramento
-----------------------------

.. figure:: imagens/peneira.png
    :width: 450px
    :align: center


"Conforme nós lemos os jornais *online*, eles também nos lêem". O website Trackography, projeto do Tactical Tech Colletive para evidenciar as empresas que rastreiam a atividade de indivíduos na Internet para montar perfis de consumo e interesses para propaganda direcionada, ilustra bem esta frase.


.. figure:: imagens/trackography.png
    :align: center

    Tela do Trackography mostrando as várias empresas, localizadas em
    diferentes países, que são acionadas quando se lê uma determinada matéria
    de um grande jornal brasileiro *online*.

Não só os jornais, mas quase todos os *websites* e aplicativos que utilizamos no dia-a-dia usam as informações que passamos voluntariamente a eles (como dados de localização, endereços de sites navegados e termos buscados) e todas as outras a que têm acesso por baixo dos panos (como detalhes de *hardware* dos nossos dispositivos, contatos e registros de chamadas) para montar o retrato mais fiel dos(as) usuários(as) -- os dados pessoais são "o combustível da economia da informação", como diz outra frase na ponta da língua dos *experts* em privacidade.

Tal proteção é uma ferramenta que deve estar disponível para todo o público, mas cujo interesse é ainda maior para crianças e adolescentes. Como `salientou durante a CPI <http://www2.camara.leg.br/atividade-legislativa/comissoes/comissoes-temporarias/parlamentar-de-inquerito/55a-legislatura/cpi-crimes-ciberneticos/documentos/audiencias-publicas/audiencia-publica-08-09.15/apresentacao-pedro-affonso-duarte-hartung>`_ Pedro Affonso Hartung, conselheiro da sociedade civil do Conanda (Conselho Nacional dos Direitos da Criança e do Adolescente), há uma série de questões relativas à coleta, integração, monetarização e comercialização de dados pessoais, visando o direcionamento de publicidade.

.. figure:: imagens/conanda_dados.png
    :align: center

.. figure:: imagens/conanda_dados2.png
    :align: center

Neste cenário, há uma forte pressão da sociedade civil, de especialistas em tecnologia e do público em geral para diminuir a quantidade de dados transmitidos na rede e proteger os que são transmitidos com tecnologias de criptografia e de anonimização de dados.

.. figure:: imagens/toranimation.png
    :align: center

.. figure:: imagens/toranimation2.png
    :align: center

    Em uma animação educativa do Projeto Tor, é explicado seu uso para proteger
    a navegação de monitoramento comercial, ao não expôr a localização nem
    detalhes do computador usado que possam identificar o(a) usuário(a).

As tecnologias de monitoramento *online* não são usadas somente por empresas. Após os vazamentos do ex-agente da CIA Edward Snowden de vários documentos internos da Agência de Segurança Nacional dos EUA (NSA), jornalistas começaram a relatar programa após programa onde a NSA e outras agências como a britânica GCHQ vigiam os principais cabos da Internet e permitem a analistas em seus quartéis (como outrora o próprio Edward Snowden) possam procurar e remontar todo o histórico de navegação e as comunicações de qualquer pessoa.

.. figure:: imagens/nsa_pontos.jpg
    :align: center

    Um mapa com os locais de interceptação de dados que a NSA possuía em 2012. Slide publicado em matéria do jornal holandês `NRC Handelsblad <http://www.nrc.nl/nieuws/2013/11/23/nsa-infected-50000-computer-networks-with-malicious-software/>`_

Tal busca é feita através de **selectors** / **seletores**. Um(a) analista pode pesquisar por atividades de um determinado endereço IP ou de e-mail, no tráfego que entra ou sai de um determinado país, ou somente das pessoas que visitaram um determinado website. Ao combinar seletores, é possível refinar a busca de forma muito poderosa.

.. figure:: imagens/selector.png
    :align: center

Desde as primeiras revelações de vigilância, em junho de 2013, muito pouco foi feito para frear tal atividade por parte das agências de inteligência e seus parceiros internacionais. Uma reação positiva, no entanto, foi um forte movimento de desenvolvimento, ensino e simplificação de **tecnologias que impedem permitem navegar sem ser identificado(a)**. Ferramentas como o Tor e VPN's permitem usar a Internet através de servidores *proxy* ou de redes voluntárias de "desidentificação", de forma que a navegação na web se assemelhe mais a uma visita à biblioteca do que a um pedido de passaporte.


.. seealso::
    :ref:`Tor e Rede Onion <tor>`

Anonimização de dados
---------------------

Outro campo onde o conceito de anonimato é não só bem-vindo como vital para a proteção das liberdades fundamentais é no processo de **anonimização** presente no **tratamento de dados pessoais**.

Com a rápida proliferação de sensores e pontos de coleta de dados, a crescente adoção de aplicativos e ferramentas *online* para interações sociais, políticas, comerciais e da vida pública, mais e mais bancos de dados são publicados e combinados com rastros digitais que guardam informações sensíveis sobre nossas vidas.

Há uma grande indústria das chamadas *data brokers*, empresas globais que compram e vendem dados sobre indivíduos para fins de segmentação de mercado. Tais repositórios corporativos de informação, quando aliados ao desenvolvimento de técnicas cada vez mais poderosas de cruzamento e mineração de dados, tornam cada vez mais fácil associar nossas ações em um determinado campo (compras no cartão, por exemplo) a ações em um campo completamente diferente (filmes favoritos).

.. figure:: imagens/associacoes.png
    :align: center

Para evitar que informações em bancos de dados possam ser usadas sem identificar os indivíduos que geraram tais dados, é então aplicado um **processo de anonimização de dados**.

Legislações e regulamentações antigas de proteção de dados pessoais, como a HIPAA que regulamenta o tratamento de dados de saúde nos EUA e a Diretiva de Proteção de Dados Pessoais da União Europeia já reconhecem que procedimentos simples de anonimização -- como a remoção de identificadores únicos como nomes e números de documento -- conferem proteção aos dados armazenados.

A mera supressão de dados que identificam alguém diretamente, no entanto, não é o suficiente para impedir a "re-identificação" -- dados aparentemente inócuos podem ser combinados para identificar uma pessoa. Um estudo clássico da pesquisadora Latanya Sweeney mostrou que é possível identificar pessoas unicamente com grande precisão sabendo-se somente o CEP, o gênero e a data de nascimento [SWEENEY-REIDENTIFY-2000]_.

Textos legislativos mais atuais sobre proteção de dados levam em conta a maneira como os dados são anonimizados; tal processo deve possuir garantias técnicas de que os indivíduos que geraram os dados não podem ser re-identificados dado o estado da arte atual de técnicas de análise computacional.


ONU: proteção à privacidade, à criptografia e ao anonimato
==========================================================

Em junho de 2015, o Relator Especial em liberdade de expressão das Nações Unidas, David Kaye, publicou seu `Report on encryption, anonymity and the human rights framework <http://www.ohchr.org/EN/HRBodies/HRC/RegularSessions/Session29/Documents/A.HRC.29.32_AEV.doc>`_ ("Relatório sobre criptografia, anonimato e o *framework* de direitos humanos").

O documento busca responder duas questões interligadas, segundo nota de lançamento do Conselho de Direitos Humanos da ONU: se os direitos à privacidade e à liberdade de opinião e expressão protegem o uso de ferramentas de criptografia e anonimato, e no caso positivo, como os governos podem impôr restrições a estas tecnologias respeitando o arcabouço legal de direitos humanos. David Kaye contou com a contribuição de 17 países e quase 30 organizações não-governamentais (`como <http://www.ohchr.org/Documents/Issues/Opinion/Communications/Jointcollaboration.pdf>`_ a Coding Rights, co-autora desta obra).

Em seu relatório, ele é enfático na proteção da criptografia e do anonimato como ferramentas para a promoção dos direitos humanos (tradução e grifos nossos):

    A respeito da criptografia e do anonimato, os Estados devem adotar
    políticas de não-restrição ou de proteção compreensiva, somente
    adotar restrições em casos específicos e atingindo objetivamente as
    demandas de legalidade, necessidade, proporcionalidade e
    legitimidade, exigir ordens judiciais para qualquer limitação
    específica, e promover a segurança e a privacidade online através da
    educação pública;

    **Leis nacionais devem reconhecer que indivíduos são livres para
    proteger a privacidade de suas comunicações digitais com o uso de
    tecnologias de criptografia e ferramentas que permitem o anonimato
    online**;

    Empresas, como os Estados, devem abster-se de bloquear ou limitar a
    transmissão de comunicações criptografadas e **permitir a comunicação
    anônima**. Atenção deve ser dada aos esforços para expandir a
    disponibilidade de *links* criptografados entre *datacenters*,
    apoiar tecnologias de segurança para websites e desenvolver
    criptografia fim-a-fim por padrão em larga escala. Atores do setor
    privado que provêem tecnologia para violar a criptografia e o
    anonimato devem ser especialmente transparentes quanto a seus
    produtos e clientes.

    O **uso de ferramentas de criptografia e anonimato** e uma melhor
    alfabetização digital **devem ser incentivados**. O Relator Especial,
    reconhecendo que **o valor de ferramentas de criptografia e anonimato
    depende de sua adoção em larga escala**, encoraja os Estados,
    organizações da sociedade civil e corporações a se engajar em uma
    campanha para trazer a criptografia por *design* e *default* para
    usuários(as) ao redor do mundo. [KAYE-2015]_


Joana Varon, co-fundadora da Coding Rights, esteve durante a 29ª sessão do Conselho de Direitos Humanos da ONU, e relatou para o Boletim Antivigilância:

    Segundo Kaye, as discussões sobre encriptação e anonimato têm sido
    polarizadas no discurso sobre seu potencial uso criminal, mas que o
    debate precisa mudar para destacar também a proteção que a
    encriptação e o anonimato proporcionam, principalmente para grupos
    que vivem em situações de risco de interferências ilegais de suas
    comunicações. [VARON-ONU-2015]_

Segundo Joana, o relatório foi bem recebido pelo Brasil -- que teve um envolvimento muito importante na defesa da privacidade na ONU, ao questionar junto com a Alemanha as práticas de vigilância em massa reveladas pelas primeiras matérias jornalísticas baseadas nos vazamentos de Edward Snowden. "Destaca-se da fala do Brasil, diante da apresentação do relatório: 'vemos valor em discutir a relevância de ferramentas de encriptação e anonimato para a proteção da privacidade e da liberdade de expressão e opinião de indivíduos'".

Anonimato é legião, porque são muitos
=====================================

Dados todos os novos nuances do conceito de "anonimato" trazidos pela Internet
como espaço livre de comunicação e ao mesmo tempo espaço onde cada ação deixa
rastros e onde a coleta e tratamento de dados é parte do modelo de negócios de
grande parte das empresas, qualquer tentativa de entender e legislar sobre o
comportamento *online* no que se refere à vedação constitucional ao anonimato
deve entender que o conceito, à época da Assembleia Constituinte, representava
apenas uma parcela dos significados que ele possui hoje em dia.

Kathleen A. Wallace publicou em 1999 uma análise chamada Anonymity ("Anonimato"), enquanto pesquisadora do Departamento de Filosofia da Universidade de Hofstra, de Nova York. Wallace define o anonimato como **"a não-coordenabilidade de traços"** -- ou seja, a dificuldade de associar dois ou mais rastros à mesma pessoa (tradução nossa):

    O anonimato é um tipo de relação entre uma pessoa anônima e outras, onde
    a primeira é conhecida somente através de um ou mais traços que não são
    coordenáveis com outros traços de modo a permitir a identificação da
    pessoa por inteiro. Considere um autor sem nome de um certo livro. Ela ou
    ele é desconhecida(o) dos outros -- leitores, por exemplo -- em
    alguns (mas não necessariamente todos) os aspectos; seu nome, suas
    relações familiares, seu endereço e etc. podem não ser conhecidos
    pelos leitores, mas o(a) autor(a) é conhecido(a) por ter escrito
    o tal livro. Então, se a descrição definitiva "autor(a) d'Os Versos
    do Capitão" não foi coordenada pelo público leitor com a pessoa
    chamada "Pablo Neruda", então o autor teria estado anônimo para tais
    pessoas (leitores) em alguns aspectos (sobrenome, endereço) em um
    determinado contexto (o público). O anonimato é sustentável à medida
    em que tal coordenação não pode ser feita (ou não sem um enorme
    esforço). [WALLACE-ANON-1999]_


A autora segue definindo melhor sua teoria do anonimato -- cujos propósitos ou objetivos podem ser divididos em três grandes categorias:

* *Agent anonymity, ou "anonimato do agente"*, com o objetivo de perpetuar as ações da pessoa anônima. Aqui, Wallace dá o exemplo de doadores e compradores, autores, fontes jornalísticas e pessoas que desejam fazer denúncias de forma anônima, além do Unabomber, indivíduo que enviava bombas para cientistas da computação de forma anônima.

* *Recipient anonymity, ou "anonimato do receptor"*, com o objetivo de prevenir ou proteger a pessoa anônima de reações. Wallace cita pessoas que se submetem a testes de HIV e outras condições de saúde estigmatizador.

* *Process anonymity, ou anonimato do processo*, com o objetivo de manter em curso algum processo. Nesta categoria entram testes duplo-cego e revisões científicas, o anonimato em pesquisas, reportagens jornalísticas e em processos judiciais como forma de manter a imparcialidade ou neutralidade de processos -- o conhecido "véu da ignorância".

Em um ensaio posterior, de 2008, para a o livro The Handbook of Information and Computer Ethics ("o livro de mão da ética da informação e dos computadores"), Kathleen A. Wallace desenvolve mais como o anonimato "pode ser exercido de diversas maneiras e que há diversos propósitos, tanto positivos quanto negativos, para os quais o anonimato pode servir, como, positivamente, promover a liberdade de expressão e a troca de ideias, ou proteger alguém de tornar-se público de forma indesejada, ou, negativamente, discursos de ódio sem responsabilização, fraudes e outras atividades criminosas" [WALLACE-ANON-2008]_.

Wallace também reconhece que "o anonimato e a privacidade são considerados como fortemente relacionados, sendo o anonimato uma das maneiras de garantir a privacidade". Entre algumas das questões onde o anonimato se apresenta como solução, ela cita **proteção contra técnicas de data mining e monitoramento** ("ao estabelecer os padrões de preferências de um(a) consumidor(a), publicitários podem fazer propagandas seletivas ou fazer sugestões para compras que são consistentes com os caminhos de interesse expressados pelo(a) usuário(a) ou por seus padrões de compras") e a **liberdade de expressão** ("uma função do anonimato pode ser permitir que um indivíduo aja ou se expresse de maneiras que não seriam possíveis ou reconhecidas se a identidade do indivíduo fosse conhecida. Por exemplo, uma escritora mulher usar um pseudônimo masculino [aqui, um pseudônimo pode de fato funcionar para garantir o anonimato] pode garantir o reconhecimento de seu trabalho que de outra maneira não teria nem ao menos sido publicado").

Na conclusão da obra, Kathleen A. Wallace despreza legislações que tratem o anonimato de modo simplista:

    Como há muitas formas de comunicações e atividades anônimas, e uma
    variedade de propósitos para as quais o anonimato pode servir, pode ser
    importante distinguir que tipo de comunicação ou atividade está
    envolvido, em vez de ter uma única política legislativa ou posição ética
    sobre o anonimato.

Por uma reinterpretação do anonimato
====================================

Especificamente no Brasil, uma tentativa interessante de reconciliar a vedação constitucional ao anonimato em publicações com seu papel vital para a manutenção da liberdade de expressão garantida pela própria Constituição é a do professor Walter Capanema, advogado e professor da Escola de Magistratura do Estado do Rio de Janeiro. Em seu ensaio O Direito ao Anonimato, Capanema pede uma "nova interpretação do anonimato" no Brasil após rever o arcabouço legal brasileiro sobre o tema e analisar seu uso corrente na Internet:

    Muito embora a literalidade do art. 5º, IV da Constituição
    Federal proíba o anonimato, tendo em vista a importância que
    esse instituto é para a salvaguarda da identidade, vida,
    liberdade e honra do indivíduo, propõe-se uma reinterpretação
    dessa norma em consonância com a própria liberdade de expressão,
    de modo a afirmar que o anonimato vedado pela Carta Magna é só
    aquele que cause prejuízos a terceiros.

    O anonimato, sem dúvida alguma é um escudo contra a tirania, de
    onde quer que ela surja.

    [...]

    A Constituição Federal, ao vedar o anonimato, estabeleceu a
    presunção de que a manifestação de vontade anônima só iria ser
    utilizada para causar prejuízos a terceiros e, com isso,
    estabeleceu uma proibição geral, ao invés de permiti-la em
    situações específicas.
    
    Não se deve admitir o anonimato como instrumento para a prática
    de crimes, especialmente os contra a honra, nem para atos que
    causem danos morais e materiais a terceiros.

    A proteção à identidade do indivíduo através do anonimato deverá
    ser consagrada em situações as quais as doações anônimas à
    caridade e a decorrente de cultos religiosos; denúncias de
    crimes, especialmente os políticos, grupos de auto-ajuda
    (Narcóticos Anônimos e Alcoólicos Anônimos, pessoas que sofreram
    abusos sexuais, pessoas com algum distúrbio ou doença e que não
    querem revelar a identidade).

    O anonimato deve ser admitido como um instrumento para a
    efetivação da liberdade de expressão, de modo a impedir ou
    evitar efeitos danos ao emitente da vontade.

    Portanto, propõe-se a reinterpretar o art. 5º, IV, CF, de forma
    a estabelecer que o anonimato ali vedado é apenas para as
    declarações de vontade que possam causar prejuízos a terceiros. [CAPANEMA-2012]_

