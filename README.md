Nota Técnica da Sociedade Civil para a CPI de Crimes Cibernéticos
=================================================================

Código-fonte da nota técnica hospedada em https://cpiciber.codingrights.org,
feita em Sphinx.

Versão atual: 1.1, publicada em 23 de abril de 2016

Sphinx e reStructuredText
-------------------------

O código-fonte do website é feito usando o sistema de documentação
[Sphinx][sphinx]; as páginas usam a sintaxe [reStructuredText][rest]. O
[reStructuredText Primer][restprimer] é um ótimo guia da linguagem.

[sphinx]: http://sphinx-doc.org/
[rest]: http://docutils.sourceforge.net/rst.html
[restprimer]: https://rest-sphinx-memo.readthedocs.org/en/latest/ReST.html

Como "compilar"
---------------

É necessário instalar o Python, o Latex (pacote texlive-full no Debian) e o
Sphinx (``easy_instal sphinx`` ou ``pip install sphinx``).

Com tudo instalado, pode-se construir a versão HTML, PDF e EPUB:

```
$ make dirhtml
```

```
$ make latexpdf
```

```
$ make epub
```

