.. _criptografia:

============
Criptografia
============

**A criptografia é uma técnica matemática que permite, entre outras coisas, garantir a confidencialidade, a autenticidade e a integridade de mensagens e documentos.**



****


Criptografia é o estudo das técnicas de se **comunicar de forma segura**
quando se tem **alguém escutando o canal de comunicação**.
Historicamente, várias formas rudimentares de substituição de letras
foram usadas para transmitir segredos entre impérios.  Hoje em dia,
qualquer técnica de criptografia relevante é feita através de
manipulações matemáticas, derivados da teoria dos números, que
transformam um bloco de informação contendo uma mensagem, imagem,
documento, etc, em um bloco de tamanho semelhante mas completamente
ininteligível.

.. figure:: imagens/cilindro_de_jefferson.jpg
    :width: 500px
    :align: center

    Um cilindro de Jefferson (ou cilindro cifrante), um dispositivo "composto
    por 26 discos de madeira que giram livremente ao redor de um eixo central
    de metal", inventado por Thomas Jefferson (que seria presidente dos EUA) em
    1795. [VICKY-JEFFERSON-2007]_

O que **impede a pessoa adversária** (nome técnico dado a quem tenta
quebrar a mensagem) **de ler a mensagem** não é o algoritmo de criptografia em
si, isto é, às instruções de como embaralhar a mensagem. Em vez de proteger o
método de embaralhamento, quem deseja sigilo para sua mensagem precisa manter
somente um pequeno, mas importante ingrediente do processo em segredo: a
**chave de criptografia** (no caso do cilindro de Jefferson, a chave é a ordem
das letras nos discos).

A isto se dá o nome de `criptografia simétrica <http://numaboa.com.br/criptografia/gerais/153-introducao>`_, pois para que os dois lados da
conversa possam trocar mensagens de forma segura, têm que combinar a chave de
antemão -- pois de nada adianta compartilhar esse segredo num canal que se
assume estar grampeado.

Com o advento da `criptografia de chave pública <http://numaboa.com.br/criptografia/chaves/835-chave-publica>`_, isso tornou-se desnecessário;
os pesquisadores Whitfield Diffie e Martin Hellman criaram um sistema onde um
**par de chaves** é gerado para cada lado da conversa; uma delas (a chave
"pública") serve para "lacrar" a mensagem; a outra (a chave "secreta" ou
"privada") serve para "abrir" tais mensagens. Dessa maneira, duas pessoas podem
**compartilhar suas chaves públicas por um canal inseguro** sem que se possa
usá-las para descriptografar as mensagens que passam em seguida. 

.. figure:: imagens/gnupg_chaves.png

    Como funciona a criptografia de chave pública. Imagem da Free Software Foundation [FSF-Jpp-GNUPG-2014]_

.. figure:: imagens/criptografia.png

    Como funciona a criptografia de chave pública. Imagem do InfoWester [INFOWESTER-2009]_

Ubíqua e vital
--------------

**Algoritmos de criptografia são blocos de construção essenciais para a segurança de nossas operações cotidianas na Internet, da infraestrutura de energia e das comunicações do governo, das transações bancárias e de mecanismos críticos do setor financeiro, e de muitas outras atividades importantes da sociedade.**

****

Quando se usa qualquer serviço moderno de comunicação pela Internet,
como Whatsapp e Gmail, ou sistemas de online banking, a criptografia
está presente. Além de prover o sigilo de mensagens, a mesma técnica
matemática pode ser usada também para autenticação: uma pessoa ou organização
pode assinar uma mensagem digitalmente usando sua chave secreta,  e quem quer
que possua sua chave pública (que pode estar publicada no *site* da
pessoa/organização) será capaz de confirmar que ela veio de fato de quem diz
ter vindo. Aplicativos que usam criptografia fim-a-fim / ponta-a-ponta
(como o Whatsapp, o Signal, o PGP e o GnuPG) criam essas chaves no próprio
aparelho das pessoas que querem se comunicar. O servidor, então, passa a
não ter acesso ao conteúdo das mensagens, embora ainda tenha a alguns
metadados (como o dia e hora da mensagem, remetente e destinatário).

.. seealso::
    :ref:`Retenção de Registros de Conexão e Aplicações > Metadados <metadados>`

.. figure:: imagens/gnupg_vigilancia.png

    Como a criptografia de chave pública protege nossas comunicações e ações rotineiras na Internet de criminosos e agências de inteligência -- no caso, com o software livre GnuPG. Imagem da Free Software Foundation [FSF-Jpp-GNUPG-2014]_

A técnica conhecida como *forward secrecy* (numa tradução livre, "sigilo
daqui para frente"), que vem sendo também implementada nos softwares de
criptografia mais modernos, consiste em criar diversas chaves, uma por
mensagem, apagando-as logo em seguida dos aparelhos. A chave secreta que
fica armazenada no dispositivo serve somente para os primeiros passos da
conexão até se estabelecer a primeira chave efêmera a ser usada na conversa.
Dessa maneira, mesmo o acesso à chave permanente não permite decifrar o
conteúdo que passa pelo servidor, e quebrar a conversa passa a envolver quebrar
dezenas ou centenas de chaves em vez de uma só.

É importante lembrar que a criptografia de chave pública e a técnica de
*forward secrecy*, embora impeçam que as mensagens retidas sejam acessíveis em
uma futura investigação policial, aumentam consideravelmente não só a
**privacidade** dos(as) usuários(as) finais frente à **indústria da coleta de
dados e da publicidade** e ao tratar de **assuntos sensíveis**, como também a
**segurança frente a fraudes** cometidas por *crackers*, o **vazamento das
informações** do servidor decorrentes de uma invasão, erro técnico ou **má
fé**, e à **sistemas de monitoramento e vigilância estrangeiros** como os
mantidos pela Agência de Segurança Nacional dos EUA e amplamente expostos na
mídia após denúncias de Edward Snowden.

.. seealso::
    :ref:`Retenção de Registros de Conexão e Aplicações > Vazamentos <vazamentos>`


Sem porta dos fundos
--------------------

**Se bem implementada, a criptografia impede que qualquer pessoa de fora da converesa acesse o conteúdo das mensagens -- até mesmo órgãos de investigação policiais ou judiciários. Alguns destes órgãos não concordam com esta possibilidade e exigem acesso especial para autoridades ou algum tipo de "chave dourada" que possa quebrar a criptografia em uma emergência.**

**Há duas possibilidades técnicas para fazer isto funcionar: um "backdoor" na criptografia ou um mecanismo de "key escrow", onde todas as mensagens criptografadas devem ser legíveis pela "chave da polícia". Se referindo a ambos os mecanismos, o Relator Especial das Nações Unidas para a Liberdade de Expressão disse em 2015 que "os Estados devem evitar todas as medidas que enfraqueçam a segurança da qual os indivíduos desfrutam online".**

****

O uso da criptografia caminha de mãos dadas com as recomendações do
Relator Especial das Nações Unidas para a Liberdade de Expressão, David
Kaye: "**os Estados devem evitar todas as medidas que enfraqueçam a
segurança da qual os indivíduos desfrutam online**, como *backdoors*,
padrões mais fracos de criptografia e *key escrows*" [KAYE-2015]_.

**Backdoors**, traduzido ao pé da letra como "porta dos fundos", são trechos
escondidos de um programa que permitem que quem o desenvolveu ganhe acesso ao
computador que o executa ou possa quebrar a sua criptografia.

Já **key escrows** são mecanismos onde a justiça ou outro poder investigativo
também possui acesso a chaves que possam abrir as mensagens. À primeira
vista, essa é uma maneira simples de resolver a questão, mas os
desdobramentos técnicos de como colocar isso em prática de forma a não
permitir que tais mecanismos sejam burlados quanto evitar abuso por
parte das autoridades de investigação trazem problemas que podem ser
ainda maiores do que a impossibilidade de fazer grampos.


Chaves debaixo do carpete
-------------------------

**Um extenso estudo sobre backdoors e key escrows feito por especialistas em computação e criptografia conclui que "tal acesso abriria portas pelas quais criminosos e nações mal intencionadas poderiam atacar os mesmos indíviduos que a polícia deseja defender".**


****

Em julho de 2015, em uma resposta à demanda de representantes do FBI e da Casa
Branca às empresas de tecnologia para que inventassem uma maneira de que a
criptografia de seus aplicativos e dispositivos pudesse ser aberta com uma
"chave dourada", disponível sob ordem judicial (semelhante ao desentendimento
entre o Whatsapp e os(as) juízes brasileiros(as) que demandaram dados de
pessoas), um grupo de especialistas em ciência da computação e criptografia se
reuniu para escrever "Keys Under Doormats: Mandating Insecurity by Requiring
Government Access to All Data and Communications" (tradução livre: "Chaves
Debaixo do Carpete: Exigindo a Insegurança ao Requerer Acesso do Governo a
Todos os Dados e Comunicações"). Em suas 32 páginas, o estudo analisa várias
formas propostas pelo governo estadunidense de tornar a criptografia quebrável
por autoridades competentes, e a conclusão é clara (tradução e grifos nossa):


    Mesmo que cidadãos necessitem dos agentes da lei para se protegerem
    no mundo digital, todos os legisladores, empresas, pesquisadores,
    indivíduos e agentes têm a obrigação de trabalhar para tornar nossa
    infraestrutura global de informações mais segura, confiável e
    resiliente. A análise deste relatório das **demandas da polícia por
    acesso excepcional às comunicações e dados privados** mostra que **tal
    acesso abriria portas pelas quais criminosos e nações mal
    intencionadas poderiam atacar os mesmos indivíduos que a polícia
    deseja defender**. Os custos seriam substanciais, os danos à inovação
    seriam severos, e as consequências no desenvolvimento econômico
    seriam difíceis de prever.


Um exemplo vivo: Juniper e o backdoor do backdoor
-------------------------------------------------

**Seis meses depois do estudo Keys Under Doormats, a gigante da tecnologia Juniper Networks anunciou que havia encontrado um backdoor em seu algoritmo de criptografia; o detalhe é que a falha, introduzida e explorada por criminosos até o momento desconhecidos, se aproveitava de OUTRA vulnerabilidade introduzida sorrateiramente pela NSA para que a agência pudesse vigiar o tráfego da rede quando necessário para seus objetivos.**


A falha, presente no sistema ScreenOS usado em equipamentos de rede NetScreen®, permitia que alguém com acesso a uma determinada *chave secreta* poderia quebrar a criptografia da VPN, usada para acessar a Internet de forma segura em redes públicas ou hostis, e ter acesso às comunicações que passam pelo cabo ou pelas ondas de rádio.

Tal esquema foi possível se aproveitando do fato de que o ScreenOS *já utilizava* um algoritmo com *backdoor*, mas neste caso implantado pela NSA: o algoritmo conhecido como "Dual-EC", que segundo o pesquisador de segurança Adam Langley "foi um esforço da NSA para introduzir um gerador de números pseudo-aleatórios com um *backdoor* que, dado o conhecimento de uma chave secreta, permitia a um adversário observar a saída do gerador e prever seus resultados futuros". Como a imprevisibilidade, a entropia, é o principal ingrediente da criptografia, esta capacidade permite quebrar facilmente a proteção da VPN.
